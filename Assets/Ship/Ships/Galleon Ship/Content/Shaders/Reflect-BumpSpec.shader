Shader "Ship/Reflected Bumped Specular" 
{
Properties 
{
	_SpecColor ("Specular Color", Color) = (0.5,0.5,0.5,1)
	_Shininess ("Shininess", Range (0.01, 15)) = 0.078125   
	_ReflectColor ("Reflection Color", Color) = (.75,.75,.75,0.5)
	_RefPower ("Reflection Power", Range(0.0,8.0)) = 3.0
	_RimColor ("Rim Color", Color) = (1,1,1,0.5)
	_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	_MainTex ("Base (RGB) RefStrGloss (A)", 2D) = "white" {}
	_Cube ("Reflection Cubemap", Cube) = "" { TexGen CubeReflect }
	
	_BumpMap ("Normalmap", 2D) = "bump" {}
}

SubShader 
{
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM
#pragma surface surf BlinnPhong 
#pragma target 3.0
//input limit (8) exceeded, shader uses 9
#pragma exclude_renderers d3d11_9x




sampler2D _MainTex;
sampler2D _BumpMap;
samplerCUBE _Cube;

fixed4 _ReflectColor,_RimColor;
fixed _Shininess, _RefPower,_RimPower;

struct Input 
{
	fixed2 uv_MainTex;
	fixed2 uv_BumpMap;
	fixed3 worldRefl;
	fixed3 viewDir;
	INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 c = tex;
	o.Albedo = c.rgb;
	o.Gloss = c.a * 2;
	o.Specular = _Shininess; 
	
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	
	fixed3 worldRefl = WorldReflectionVector (IN, o.Normal);
	fixed4 reflcol = texCUBE (_Cube, worldRefl);
	reflcol *= tex.a;
	
	
	fixed rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
	
	o.Albedo += (pow (rim, _RimPower) * ((_RimColor - .5) * 2)* c.a );
	
	o.Emission = reflcol.rgb * _ReflectColor.rgb *  pow (rim, _RefPower) *  (_ReflectColor.a * 10.0) ;
	
	o.Alpha = reflcol.a * _ReflectColor.a;
}
ENDCG
}

FallBack "Reflective/Bumped Diffuse"
}
