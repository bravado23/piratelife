Shader "Ship/Sail" 
{
Properties
{
            _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
            _Shininess ("Shininess", Range (0.03, 1)) = 0.8
            _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
            _MainTex2 ("BaseBack (RGB) Gloss (A)", 2D) = "white" {}
            _BumpMap ("Bumpmap", 2D) = "bump" {}
            _BumpMap2 ("Bumpmap2", 2D) = "bump" {}
            _LightRadius("Light source radius", float) = 2
            _fFalloffPower("Falloff", float) = 2.0
            _fLightSharpness("Light Sharpness", float) = 16.0
            _LightSoftness ("Light Softness", Range (0,1)) = 0.2
            _WindSpeed ("_WindSpeed", float) = 2.0
            _Push ("_Push", float) = 0.01
            
            
}

SubShader
{
	Tags { "RenderType" = "Opaque" }
	cull back
	CGPROGRAM
	#pragma  surface surf SubSurface   addshadow  vertex:disp exclude_path:prepass
	#pragma target 3.0
	
	fixed _Shininess;
	
	
	fixed _LightRadius;
	fixed _fFalloffPower;
	fixed _fLightSharpness;
	fixed _fLinearAttenuation;
	fixed _LightSoftness;
	fixed _Cutoff;
	fixed _WindSpeed;
	fixed _Push;
	
	

	struct MySurfOutput 
	{
		half3 Albedo;
		half3 Normal;
		half3 Emission;
		half Specular;
		half Gloss;
		half Alpha;
		fixed3 Scattering;
	};
	
	fixed4 LightingSubSurface (MySurfOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
	{		
		fixed diff = max (0, dot (s.Normal, lightDir));		
		// ШАГ 1 : смягчаем тени
		fixed diffusn = diff;
		diff = (1 - _LightSoftness + _LightSoftness * diff);
		
		// ШАГ 2 : рассеивание
		fixed fLightDistance = max( 0.0, length(lightDir) - _LightRadius );
		fixed fScattering = min( 1.0, 2.5 / (1.0 + fLightDistance  * atten ) );                 
		fScattering = pow( fScattering, _fFalloffPower );
		
		// ШАГ 3 : вычисляем проходящий сквозь объект свет
		fixed3 vLightTovertex = normalize(lightDir);
		fixed3 vViewTovertex  = normalize(-viewDir);
		fixed VdotL  = max(0.0, dot(vLightTovertex, vViewTovertex));
		VdotL  = pow(VdotL, _fLightSharpness);
		diff += VdotL;
		
		s.Scattering = lerp(diffusn, diff * fScattering, s.Scattering) ;
		
		fixed4 c;
		c.rgb = (s.Albedo * s.Scattering) * (atten * 2) * _LightColor0.rgb;
		c.a = s.Alpha + _LightColor0.a  * atten;
		return c;
	}
		
	fixed4 LightingSubSurface_PrePass (MySurfOutput s, fixed4 light)
	{
		fixed4 c;
		c.rgb = s.Albedo * light.rgb * s.Alpha;
		c.a = s.Alpha;
		return c;
	}
		
		
	sampler2D _MainTex,_BumpMap,_BumpMap2;
	
	
	struct appdata 
	{
		fixed4 vertex : POSITION;
		fixed4 tangent : TANGENT;
		fixed3 normal : NORMAL;
		fixed2 texcoord : TEXCOORD0;
		fixed2 texcoord1 : TEXCOORD1;
	};
	
	
	struct Input 
	{
		fixed2 uv_MainTex;
		fixed2 uv_BumpMap;
		fixed2 uv_BumpMap2;
		
	};
	
	
	
	void disp (inout appdata v)
	{
	  
	}
	
	void surf (Input IN, inout MySurfOutput o) 
	{
		IN.uv_BumpMap2.y += _Time * _WindSpeed;
		
		fixed3 n2 =  UnpackNormal (tex2D (_BumpMap2, IN.uv_BumpMap2));
		IN.uv_MainTex.y += n2.g * _Push;
		
		fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
		fixed3 n1 =  UnpackNormal (tex2D (_BumpMap, IN.uv_MainTex));
		o.Albedo = tex.rgb;
		o.Alpha = 1;
		o.Scattering = tex.a;
		o.Normal = lerp(n1,n2,.5);
	}
	
	ENDCG
	

	cull Front
	CGPROGRAM
	#pragma  surface surf SubSurface   addshadow  vertex:disp exclude_path:prepass 
	#pragma target 3.0
	
	fixed _Shininess;
	
	
	fixed _LightRadius;
	fixed _fFalloffPower;
	fixed _fLightSharpness;
	fixed _fLinearAttenuation;
	fixed _LightSoftness;
	fixed _Cutoff;
	fixed _WindSpeed;
	fixed _Push;
	
	

	struct MySurfOutput 
	{
		half3 Albedo;
		half3 Normal;
		half3 Emission;
		half Specular;
		half Gloss;
		half Alpha;
		fixed3 Scattering;
	};
	
	fixed4 LightingSubSurface (MySurfOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
	{		
		fixed diff = max (0, dot (s.Normal, lightDir));		
		// ШАГ 1 : смягчаем тени
		fixed diffusn = diff;
		diff = (1 - _LightSoftness + _LightSoftness * diff);
		
		// ШАГ 2 : рассеивание
		fixed fLightDistance = max( 0.0, length(lightDir) - _LightRadius );
		fixed fScattering = min( 1.0, 2.5 / (1.0 + fLightDistance  * atten ) );                 
		fScattering = pow( fScattering, _fFalloffPower );
		
		// ШАГ 3 : вычисляем проходящий сквозь объект свет
		fixed3 vLightTovertex = normalize(lightDir);
		fixed3 vViewTovertex  = normalize(-viewDir);
		fixed VdotL  = max(0.0, dot(vLightTovertex, vViewTovertex));
		VdotL  = pow(VdotL, _fLightSharpness);
		diff += VdotL;
		
		s.Scattering = lerp(diffusn, diff * fScattering,s.Scattering) ;
		
		fixed4 c;
		c.rgb = (s.Albedo * s.Scattering) * (atten * 2) * _LightColor0.rgb;
		c.a = s.Alpha + _LightColor0.a  * atten;
		return c;
	}
		

		
	sampler2D _MainTex2,_BumpMap,_BumpMap2;
	
	
	struct appdata 
	{
		fixed4 vertex : POSITION;
		fixed4 tangent : TANGENT;
		fixed3 normal : NORMAL;
		fixed2 texcoord : TEXCOORD0;
		fixed2 texcoord1 : TEXCOORD1;
	};
	
	
	struct Input 
	{
		fixed2 uv_MainTex;
		fixed2 uv_BumpMap;
		fixed2 uv_BumpMap2;
		
	};
	
	
	
	void disp (inout appdata v)
	{
	  v.normal.xyz *= -1;
	}
	
	void surf (Input IN, inout MySurfOutput o) 
	{
		IN.uv_BumpMap2.y += _Time * _WindSpeed;
		
		fixed3 n2 =  UnpackNormal (tex2D (_BumpMap2, IN.uv_BumpMap2));
		IN.uv_MainTex.y += n2.g * _Push;
		
		fixed4 tex = tex2D(_MainTex2, IN.uv_MainTex);
		fixed3 n1 =  UnpackNormal (tex2D (_BumpMap, IN.uv_MainTex));
		o.Albedo = tex.rgb;
		o.Alpha = 1;
		o.Scattering = tex.a;
		o.Normal = (n1 + n2 ) * .5;
	}
	
	ENDCG
	

}


	
}


 
