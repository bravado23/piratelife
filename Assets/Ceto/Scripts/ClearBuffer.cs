﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ClearBuffer : MonoBehaviour {

    public new Camera camera;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       camera.RemoveCommandBuffers(CameraEvent.AfterDepthTexture);
       

    }
}
