﻿using UnityEngine;
using System.Collections;

namespace Hessburg 
{
	public class CannonFX : MonoBehaviour 
	{
		public Light Muzzlelight;
		public ParticleSystem CannonFire;
		public float LastFired;
		private float LightTimer;
		private float MuzzleLightDuration;
		private float CurrentIntensity;
		public bool firing;
		public AudioSource CarronadeSound;

		void Awake()
		{
			MuzzleLightDuration=0.6f;
		}
			
		public void FireCannon(float ReloadTime) 
		{
			if(LastFired+ReloadTime < Time.time)
			{
				LightTimer = Time.time;
				Muzzlelight.enabled=true;
				firing=true;
				CannonFire.Stop(true);
				CannonFire.Clear(true);
				CannonFire.Play(true);
				CarronadeSound.pitch=Random.Range(0.8f, 1.0f);
				CarronadeSound.Play();
				LastFired=Time.time;
			}	
		}

		public void FireUpdate()
		{	
			if(firing)
			{
				if(LightTimer+MuzzleLightDuration>Time.time)
				{
					Muzzlelight.intensity=2.0f*(1.0f-(Time.time-LightTimer)*(1.0f/MuzzleLightDuration));
				}
				else
				{
					Muzzlelight.enabled=false;
					firing=false;
				}	
			}	
		}
			
	}
}