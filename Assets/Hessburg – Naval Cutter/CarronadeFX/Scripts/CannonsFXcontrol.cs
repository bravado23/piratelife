﻿using UnityEngine;
using System.Collections;

// The cutter has 14 carronades (12 pound) – a reasonable reload time would be 27 seconds.

namespace Hessburg 
{
	public class CannonsFXcontrol : MonoBehaviour 
	{
		public CutterControl CutterControl;
		public float ReloadTime; 
		public float BroadsideMaximumDelay;
		public CannonFX[] Cannon;

		public Material SmokeHuge;
		public Material SmokeSmall;
		public Shader SmokeSoftParticles;
		public Shader Smoke;
		public Shader SmokePre55;
		#if UNITY_5_5_OR_NEWER
		private bool SPon;
		#endif

		private float[] BroadsideTimerStarboard;
		private float BroadsideCommandTimeStarboard;
		private int BroadsideCounterStarboard;
		private bool BroadsideStarboard;
		private bool[] BroadsideCannonFiredStarboard;
		private float[] BroadsideTimerPort;
		private float BroadsideCommandTimePort;	
		private int BroadsideCounterPort;
		private bool BroadsidePort;
		private bool[] BroadsideCannonFiredPort;

		public GameObject CarronadeFX_Port;
		public GameObject CarronadeFX_Starboard;

		void Start () 
		{
			BroadsideTimerStarboard = new float[7];
			BroadsideTimerPort = new float[7];
			BroadsideCannonFiredStarboard = new bool[7];
			BroadsideCannonFiredPort = new bool[7];
			for(int i=0; i<14;i++)
			{	
				Cannon[i].LastFired=-ReloadTime;
				Cannon[i].transform.parent=CutterControl.carronadeVertical[i];
			}
			CutterControl.CannonsFXcontrol=this;
			Destroy(CarronadeFX_Port);
			Destroy(CarronadeFX_Starboard);

			#if UNITY_5_5_OR_NEWER
				SPon=QualitySettings.softParticles;
				if(SPon==true)
				{
					SmokeHuge.shader=SmokeSoftParticles;
					SmokeSmall.shader=SmokeSoftParticles;
				}
				else
				{
					SmokeHuge.shader=Smoke;
					SmokeSmall.shader=Smoke;
				}	
			#else
				SmokeSmall.shader=SmokePre55;
				SmokeHuge.shader=SmokePre55;
			#endif
		}
		
		
		void Update () 
		{
			int i;

			#if UNITY_5_5_OR_NEWER
			if(SPon!=QualitySettings.softParticles) // changes shader of the smoke materials – necessary because the shader uses soft particles without ZTesting (found no way to turn ZTest on in shader if softparticles are off)
			{
				SPon=QualitySettings.softParticles;
				if(SPon==true)
				{
					SmokeHuge.shader=SmokeSoftParticles;
					SmokeSmall.shader=SmokeSoftParticles;
				}
				else
				{
					SmokeHuge.shader=Smoke;
					SmokeSmall.shader=Smoke;
				}	
			}
			#endif

			if(BroadsideStarboard==true)
			{
				for(i=0; i<7;i++)
				{	
					if(Time.time-BroadsideCommandTimeStarboard>BroadsideTimerStarboard[i] && BroadsideCannonFiredStarboard[i]==false)
					{
						FireCannonNumber(i);
						BroadsideCannonFiredStarboard[i]=true;
						BroadsideCounterStarboard++;
					}	
				}	
				if(BroadsideCounterStarboard>=7) BroadsideStarboard=false;
			}	

			if(BroadsidePort==true)
			{
				for(i=0; i<7;i++)
				{	
					if(Time.time-BroadsideCommandTimePort>BroadsideTimerPort[i] && BroadsideCannonFiredPort[i]==false)
					{
						FireCannonNumber(i+7);
						BroadsideCannonFiredPort[i]=true;
						BroadsideCounterPort++;
					}	
				}	
				if(BroadsideCounterPort>=7) BroadsidePort=false;
			}	

			for(i=0; i<14;i++)
			{	
				if(Cannon[i].firing)
				{
					Cannon[i].FireUpdate();
				}
			}	

		}

		public void FireCannonNumber(int number) 
		{
			Cannon[number].FireCannon(ReloadTime);
		}
		
		public void FireBroadsideStarboard()
		{
			InitBroadsideStarboard();
		}
		
		public void FireBroadsidePort()
		{
			InitBroadsidePort();
		}

		void InitBroadsideStarboard() 
		{	
			BroadsideCommandTimeStarboard=Time.time;
			for(int i=0; i<7;i++)
			{
				BroadsideCannonFiredStarboard[i]=false;
				BroadsideTimerStarboard[i]=Random.Range(0.0f, BroadsideMaximumDelay);
			}	
			BroadsideCounterStarboard=0;
			BroadsideStarboard=true;
		}

		void InitBroadsidePort() 
		{	
			BroadsideCommandTimePort=Time.time;
			for(int i=0; i<7;i++)
			{
				BroadsideCannonFiredPort[i]=false;
				BroadsideTimerPort[i]=Random.Range(0.0f, BroadsideMaximumDelay);
			}	
			BroadsideCounterPort=0;
			BroadsidePort=true;
		}


	}
}	
