﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "Hessburg/SmokeShader(pre Unity 5.5)"
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {} 
		_Density ("SmokeDensity", Range(0.01,4.0)) = 1.0
		_NormalStrength ("Normal Strength", Range(0.0,5.0)) = 1.0		
	}

	SubShader 
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	    Blend SrcAlpha OneMinusSrcAlpha
	    ColorMask RGB
	    Cull Back
	    ZWrite Off  

	// ---- forward rendering base pass:
	Pass 
	{
		Name "FORWARD"
		Tags { "LightMode" = "ForwardBase" }
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		#pragma multi_compile_fog
		#pragma multi_compile_fwdbasealpha noshadow nolightmap nodirlightmap nodynlightmap	
		#pragma multi_compile_particles
		#pragma multi_compile _ SOFTPARTICLES_ON
		#include "UnityShaderVariables.cginc"
		#define UNITY_PASS_FORWARDBASE
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#include "AutoLight.cginc"

		float4 _MainTex_ST;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		float _Density;
		float _NormalStrength;

		#ifdef SOFTPARTICLES_ON
			sampler2D_float _CameraDepthTexture;
		#endif

		struct v2f 
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0; 
			half3 worldNormal : TEXCOORD1;
			fixed3 vlight : TEXCOORD2; 
			UNITY_FOG_COORDS(3)
			float3 worldPos : TEXCOORD4;   
			fixed4 color : COLOR0;

			#ifdef SOFTPARTICLES_ON
				float4 projPos : TEXCOORD5;
			#endif
		};

		v2f vert (appdata_full v) 
		{
			v2f o;
			UNITY_INITIALIZE_OUTPUT(v2f, o);

			o.pos = UnityObjectToClipPos(float4(v.vertex.xyz, 1.0));
            o.color = v.color;
			o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex); 
			o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
			o.worldNormal = UnityObjectToWorldNormal(v.normal);

			#if UNITY_SHOULD_SAMPLE_SH
				float3 shlight = ShadeSH9 (float4(o.worldNormal,1.0));
				o.vlight = shlight;
			#else		
				o.vlight = 0.0;
			#endif

			#ifdef SOFTPARTICLES_ON
				o.projPos = ComputeScreenPos (o.pos);
				COMPUTE_EYEDEPTH(o.projPos.z);
			#endif

			UNITY_TRANSFER_FOG(o, o.pos);
			return o;
		}

		fixed4 frag (v2f i) : SV_Target 
		{		
			#ifdef SOFTPARTICLES_ON
				float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
				float partZ = i.projPos.z; // use this one if ztest is on
				//float partZ = i.projPos.z - (0.5 / 0.16);
				float fade = saturate (0.16 * (sceneZ-partZ));
				i.color.a *= fade;
			#endif

			float3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));
			float3 worldPos = i.worldPos;
			float3 dp1 = ddx(worldPos);
			float3 dp2 = ddy(worldPos)*_ProjectionParams.x;

			#if UNITY_UV_STARTS_AT_TOP
				float2 duv1 = ddx(i.uv);
				float2 duv2 = ddy(i.uv) * _ProjectionParams.x;
			#else
				float2 duv1 = ddx(1.0-i.uv);
				float2 duv2 = ddy(1.0-i.uv) * _ProjectionParams.x;
			#endif

			float3 dp2perp = cross(dp2, i.worldNormal);
			float3 dp1perp = cross(i.worldNormal, dp1);
			float3 T = dp2perp*duv1.x+dp1perp*duv2.x;
			float3 B = dp2perp*duv1.y+dp1perp*duv2.y;
			float invmax = rsqrt(max(dot(T,T), dot(B,B)));
			float3 worldNormal = normalize(mul(normal, float3x3(T*invmax, B*invmax, i.worldNormal))) * _NormalStrength;      


			UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos) 

			fixed4 c = tex2D(_MainTex, i.uv.xy) * _Color;
			c.rgb = c.rgb * i.vlight;
			half3 Albedo = c.rgb;
			half Alpha = c.a * i.color.a;

			half NdotL = dot(worldNormal, _WorldSpaceLightPos0.xyz);

			Albedo = (Albedo * ( (_LightColor0.rgb * (NdotL * atten) ) + ShadeSH9(float4(worldNormal,1) ) ) ) * i.color.rgb;
			c.rgb += Albedo;

			c.a = clamp(Alpha*_Density, 0.0, 1.0);

			UNITY_APPLY_FOG(i.fogCoord, c);
			
			return c;
		}
	ENDCG
	}
		// ---- forward rendering additive lights pass:
		Pass {
		Name "FORWARD"
		Tags { "LightMode" = "ForwardAdd" }
		ZWrite Off Blend One One
		Blend SrcAlpha One

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		#pragma multi_compile_fog
		#pragma multi_compile_fwdadd nolightmap nodirlightmap nodynlightmap noshadow 
		#pragma multi_compile_particles
		#pragma multi_compile _ SOFTPARTICLES_ON
		#define UNITY_PASS_FORWARDADD
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#include "AutoLight.cginc"
	
		float4 _MainTex_ST;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		float _NormalStrength;

		#ifdef SOFTPARTICLES_ON
			sampler2D_float _CameraDepthTexture;
		#endif

		struct v2f
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			half3 worldNormal : TEXCOORD1;
			float3 worldPos : TEXCOORD2;

			#ifdef SOFTPARTICLES_ON
				float4 projPos : TEXCOORD5;
			#endif

			UNITY_FOG_COORDS(3)
		};

		v2f vert (appdata_full v)
		{
			v2f o;
			UNITY_INITIALIZE_OUTPUT(v2f,o);
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
			float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
			fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
			o.worldPos = worldPos;
			o.worldNormal = worldNormal;
			UNITY_TRANSFER_FOG(o,o.pos);

			#ifdef SOFTPARTICLES_ON
				o.projPos = ComputeScreenPos (o.pos);
				COMPUTE_EYEDEPTH(o.projPos.z);
			#endif

			return o;
		}

		fixed4 frag (v2f i) : SV_Target 
		{
			#ifdef SOFTPARTICLES_ON
				float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
				float partZ = i.projPos.z; // use this one if ztest is on
				//float partZ = i.projPos.z - (0.5 / 0.16);
				float fade = saturate (0.16 * (sceneZ-partZ));
			#endif

			#ifndef USING_DIRECTIONAL_LIGHT
				fixed3 lightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
			#else
				fixed3 lightDir = _WorldSpaceLightPos0.xyz;
			#endif

			float3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));

            float3 dp1 = ddx(i.worldPos);
            float3 dp2 = ddy(i.worldPos)*_ProjectionParams.x;

            #if UNITY_UV_STARTS_AT_TOP
                float2 duv1 = ddx(i.uv);
                float2 duv2 = ddy(i.uv) * _ProjectionParams.x;
            #else
                float2 duv1 = ddx(1.0-i.uv);
                float2 duv2 = ddy(1.0-i.uv) * _ProjectionParams.x;
            #endif

            float3 dp2perp = cross(dp2, i.worldNormal);
            float3 dp1perp = cross(i.worldNormal, dp1);
            float3 T = dp2perp*duv1.x+dp1perp*duv2.x;
            float3 B = dp2perp*duv1.y+dp1perp*duv2.y;
            float invmax = rsqrt(max(dot(T,T), dot(B,B)));
            float3 worldNormal = normalize(mul(normal, float3x3(T*invmax, B*invmax, i.worldNormal))) * _NormalStrength;      

  			UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos)
  	
  			fixed4 c = tex2D (_MainTex, i.uv.xy);

			#ifdef DIRECTIONAL
				half NdotL = dot (worldNormal, lightDir);			
				c.rgb = c.rgb * _LightColor0.rgb * (NdotL * atten) * 0.125;
			#else
				c.rgb = c.rgb * _LightColor0.rgb * atten * 0.125;	
			#endif

			#ifdef SOFTPARTICLES_ON
			c.a=c.a*fade;
			#endif
			UNITY_APPLY_FOG(i.fogCoord, c);
			return c;
		}
	ENDCG
	}
}

	FallBack "Diffuse"
}
