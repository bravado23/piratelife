Shader "Hessburg/Double Sided Sails" {

	Properties
	{
		_MainTex("Albedo", 2D) = "white" {}
		_MetallicGlossMap("Metallic", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_OcclusionMap("Occlusion", 2D) = "white" {}	
	}

    SubShader {
        Tags { "RenderType"="Opaque" "PerformanceChecks"="False" }
        LOD 300
        ZWrite On
        Cull Off
   
        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows nolightmap addshadow fullforwardshadows //addshadow
        #pragma target 3.0
        sampler2D _MainTex;
        sampler2D _MetallicGlossMap;
        sampler2D _BumpMap;
        sampler2D _OcclusionMap;

        struct Input 
        {
            float2 uv_MainTex;
            fixed vface : VFACE;
        };

        half _Metallic;
        half _BumpScale;
    
 
        void surf (Input IN, inout SurfaceOutputStandard o) 
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
 
            fixed4 metallic = tex2D(_MetallicGlossMap, IN.uv_MainTex);
            o.Metallic = metallic.r;
            o.Smoothness = metallic.a;
            
            o.Occlusion = tex2D(_OcclusionMap, IN.uv_MainTex).g;

            //o.Normal = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_MainTex), 1.0);
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
            o.Normal.z *= IN.vface;
        }
        ENDCG
    }
    FallBack "VertexLit"
}
 