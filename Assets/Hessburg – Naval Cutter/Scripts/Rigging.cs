﻿using UnityEngine;
using System.Collections;

namespace Hessburg 
{
	public class Rigging : MonoBehaviour 
	{
		private LineRenderer[] lineRenderer;
		private bool ropesInitialized;
		public bool receiveShadows;
		public UnityEngine.Rendering.ShadowCastingMode castShadows;
		#if UNITY_5_4
		public bool motionVectors;
		#endif
		public Material material;

		public float width;
		public bool[] RenderRope;
		public Transform[] RopeStartPosition;
		public Transform[] RopeEndPosition;


		void Start()
		{	
				if(RopeStartPosition.Length == RopeEndPosition.Length)
				{
					if(RopeStartPosition.Length>0)
					{
						GameObject[] rope;
						rope = new GameObject[RopeStartPosition.Length];
						lineRenderer = new LineRenderer[RopeStartPosition.Length];
						for(int i = 0;i<RopeStartPosition.Length;i++)
						{				
							rope[i] = new GameObject("Rope "+i);
							rope[i].transform.parent = transform;					
							lineRenderer[i] = rope[i].AddComponent( typeof(LineRenderer) ) as LineRenderer;
							lineRenderer[i].useWorldSpace = true;
							lineRenderer[i].material = material;	

							#if UNITY_5_6_OR_NEWER
								lineRenderer[i].startWidth=width;
								lineRenderer[i].endWidth=width;
								lineRenderer[i].positionCount=2;
							#endif

							#if UNITY_5_5
								lineRenderer[i].startWidth=width;
								lineRenderer[i].endWidth=width;
								lineRenderer[i].numPositions=2;
							#endif

							#if UNITY_5_4
								lineRenderer[i].SetWidth(width,width);
								lineRenderer[i].SetVertexCount(2);
							#endif

							lineRenderer[i].SetPosition(0, RopeStartPosition[i].position);	
							lineRenderer[i].SetPosition(1, RopeEndPosition[i].position);		
							lineRenderer[i].receiveShadows=receiveShadows;
							lineRenderer[i].shadowCastingMode=castShadows;
							#if UNITY_5_4
							lineRenderer[i].motionVectors=motionVectors;
							#endif
						}
						ropesInitialized=true;	
					}	
				}
				else
				{
					Debug.Log("Warning: Skipped creating ropes – the number of 'Rope Start Positions' and 'Rope End Positions' does not match!");
					ropesInitialized=false;
				}			
		}


		void LateUpdate()
		{	
			if(ropesInitialized)
			{
				for(int i = 0;i<lineRenderer.Length;i++)
				{
					lineRenderer[i].enabled=RenderRope[i];
					lineRenderer[i].SetPosition(0, RopeStartPosition[i].position);	
					lineRenderer[i].SetPosition(1, RopeEndPosition[i].position);
				}		
			}		
		}


	}
}