﻿using UnityEngine;
using System.Collections;

/*
		(C) 2016-2017 Mark Hessburg

		Numbers and translation for the names of the different sails (I'm not a sailor, so I've took them from french websites when naming the model parts): 

		0 – Foc: Jib
		1 – Foc en l'air: Jib Topsail
		2 – Trinquette: Staysail
		3 – Grand Voile: Mainsail
		4 – Fortune: Square Sail
		5 – Hunier: Top Sail
		6 – Perroquet: Topgallant Sail
		7 – Studding sail port
		8 – Studding sail starboard

*/

namespace Hessburg 
{
	public class CutterControl : MonoBehaviour 
	{
		public Rigging Rigging;
		public bool startWithSailsSet;
		public bool windDirectionFollowsShipRotation; // for a quick and simple usage - wind will always blow from behind the ship
		public bool useDeckColliders; // will activate simple deck colliders
		public Transform cutterTransform;
		private Vector3 windDirection;
		private Vector3 randomWindDirection;
		private Vector3 windDirectionFlag;
		private Vector3 randomWindDirectionFlag;
		private float windSpeed; 
		private float windD;
		private float[] windBlend;
		public float SailAnimationVelocity=1.0f;
		public float SailClothDamping=0.75f; // you might want to increase that to 1.0 at unrealistically high velocities to eliminate visible stutter of the sails.

		public float[] carronadeHorizontalAngle;
		public float[] carronadeVerticalAngle;
		private bool[] openGunPortLid; // will close a lid if set to false
		private bool[] gunPortLidOpened;
		private bool[] gunPortLidClosed;
		private float[] lidOpeningAmount; // range 0-1
		private float[] upperLidTargetL;
		private float[] lowerLidTargetL;
		private float[] upperLidTargetR;
		private float[] lowerLidTargetR;
		private float[] LidAngularVelocity;

		private GameObject[] sailLOD0;
		private GameObject[] sailLOD1;
		private GameObject[] sailLOD2;
		private GameObject[] sailBSLOD0;
		private GameObject[] sailBSLOD1;
		private GameObject[] sailBSLOD2;

		private SkinnedMeshRenderer[] sailRendererLOD0;
		private SkinnedMeshRenderer[] sailRendererLOD1;
		private SkinnedMeshRenderer[] sailRendererLOD2;

		private int[] blendShapeCountLOD0;
	    private SkinnedMeshRenderer[] skinnedMeshRendererLOD0;
	    private Mesh[] skinnedMeshLOD0;
	    private int[] blendShapeCountLOD1;
	    private SkinnedMeshRenderer[] skinnedMeshRendererLOD1;
	    private Mesh[] skinnedMeshLOD1;
	    private int[] blendShapeCountLOD2;
	    private SkinnedMeshRenderer[] skinnedMeshRendererLOD2;
	    private Mesh[] skinnedMeshLOD2;

	    private Cloth[] SailClothLOD0;
	    private Cloth[] SailClothLOD1;
	    private Cloth[] SailClothLOD2;

	    public GameObject FlagLOD0;
	    public GameObject FlagLOD1;
	    public GameObject FlagLOD2;
	    private Cloth FlagClothLOD0;
	    private Cloth FlagClothLOD1;
	    private Cloth FlagClothLOD2;

	    private bool[] setSailTransition;
		private bool[] setSail;
		private bool[] sailSet;
		private bool[] sailDown;
		private float[] sailSetAmount;
		private float[] sailSetVelocity;	

		public Transform CompassNeedle;

		public Transform yardarmParentRotation;
		public Transform rearYardarmParentRotation;
		public Transform focParentRotation;
		public Transform focEnLAirParentRotation;
		public Transform trinquetteParentRotation;
		public Transform studdingSailYardarmPortParent;
		private float studdingSailYardarmPortParentPositionX;
		public Transform studdingSailYardarmStarboardParent;
		private float studdingSailYardarmStarboardParentPositionX;

		public Transform studdingYardarmPortLOD0;
		public Transform studdingYardarmStarboardLOD0;
		public Transform studdingYardarmPortLOD1;
		public Transform studdingYardarmStarboardLOD1;
		public Transform studdingYardarmPortLOD2;
		public Transform studdingYardarmStarboardLOD2;

		public Transform yardarmLOD0;
		public Transform yardarmLOD1;
		public Transform yardarmLOD2;
		public Transform yardarmBlendshapesLOD0;
		public Transform yardarmBlendshapesLOD1;
		public Transform yardarmBlendshapesLOD2;

		public Transform rearYardarmLOD0;
		public Transform rearYardarmLOD1;
		public Transform rearYardarmLOD2;
		public Transform rearYardarmBlendshapesLOD0;
		public Transform rearYardarmBlendshapesLOD1;
		public Transform rearYardarmBlendshapesLOD2;

		public Transform focLOD0;
		public Transform focEnLAirLOD0;
		public Transform trinquetteLOD0;
		public Transform grandVoileLOD0;
		public Transform fortuneLOD0;
		public Transform hunierLOD0;
		public Transform perroquetLOD0;
		public Transform studdingSailPortLOD0;
		public Transform studdingSailStarboardLOD0;

		public Transform focLOD1;
		public Transform focEnLAirLOD1;
		public Transform trinquetteLOD1;
		public Transform grandVoileLOD1;
		public Transform fortuneLOD1;
		public Transform hunierLOD1;
		public Transform perroquetLOD1;
		public Transform studdingSailPortLOD1;
		public Transform studdingSailStarboardLOD1;

		public Transform focLOD2;
		public Transform focEnLAirLOD2;
		public Transform trinquetteLOD2;
		public Transform grandVoileLOD2;
		public Transform fortuneLOD2;
		public Transform hunierLOD2;
		public Transform perroquetLOD2;
		public Transform studdingSailPortLOD2;
		public Transform studdingSailStarboardLOD2;

		public Transform focBlendShapesLOD0;
		public Transform focEnLAirBlendShapesLOD0;
		public Transform trinquetteBlendShapesLOD0;
		public Transform grandVoileBlendShapesLOD0;
		public Transform fortuneBlendShapesLOD0;
		public Transform hunierBlendShapesLOD0;
		public Transform perroquetBlendShapesLOD0;
		public Transform studdingSailPortBlendShapesLOD0;
		public Transform studdingSailStarboardBlendShapesLOD0;

		public Transform focBlendShapesLOD1;
		public Transform focEnLAirBlendShapesLOD1;
		public Transform trinquetteBlendShapesLOD1;
		public Transform grandVoileBlendShapesLOD1;
		public Transform fortuneBlendShapesLOD1;
		public Transform hunierBlendShapesLOD1;
		public Transform perroquetBlendShapesLOD1;
		public Transform studdingSailPortBlendShapesLOD1;
		public Transform studdingSailStarboardBlendShapesLOD1;

		public Transform focBlendShapesLOD2;
		public Transform focEnLAirBlendShapesLOD2;
		public Transform trinquetteBlendShapesLOD2;
		public Transform grandVoileBlendShapesLOD2;
		public Transform fortuneBlendShapesLOD2;
		public Transform hunierBlendShapesLOD2;
		public Transform perroquetBlendShapesLOD2;
		public Transform studdingSailPortBlendShapesLOD2;
		public Transform studdingSailStarboardBlendShapesLOD2;

		public Transform[] carronadeHorizontal;
		public Transform[] carronadeVertical;
		public Transform[] carronadeSlider;
		public Transform[] gunPortUpperLidLOD0;
		public Transform[] gunPortLowerLidLOD0;
		public Transform[] gunPortUpperLidLOD1;
		public Transform[] gunPortLowerLidLOD1;

		// Some ropes will be moved when sails are set:
		private Vector3 RopeEndPosA;
		private Vector3 RopeEndPosB;
		private Vector3 RopeEndPos1A;
		private Vector3 RopeEndPos1B;
		private Vector3 RopeEndPos2A;
		private Vector3 RopeEndPos2B;
		private Vector3 RopeEndPos13A;
		private Vector3 RopeEndPos13B;
		private Vector3 RopeEndPos14A;
		private Vector3 RopeEndPos14B;

		public Transform rudderLever;
		public Transform rudderWheelParent;
		public Transform rudder;
		public SkinnedMeshRenderer rudderRopeBlendShape;

		public GameObject DeckColliderParent;

		public float adjustNorth;

		private Vector3 raisingEndPosition;
		private Vector3 loweringEndPosition;
		private Vector3 raisingRope2Position;
		private Vector3 loweringRope2Position;
		private Vector3 raisingRope0Position;
		private Vector3 loweringRope0Position;

		private	Vector3 frontSailMasterAngle;
		private	Vector3 Retracted;

		private bool oldUseDeckColliders;

		public bool sailsLOD0_only; // uses sails of LOD0 only – this eliminates flickering of sails due to LOD transitions on the cost of higher vertices in LODs. 
		//WARNING: Generally keep this as is! Don't set this bool to true when using the prefab 'Naval Cutter – Sails more LODs'! And don't set it to false when using the prefab 'Naval Cutter'!

		// new in 1.1:
		public CannonsFXcontrol CannonsFXcontrol;

		void Awake () 
		{	
			MoveableRopesPositionSetup();
			ReparentSails();	
			ResetSails();
			ResetGunPorts();	
		}
		
		void Start()
		{
			WindSpeed(0.2f);
			if(windDirectionFollowsShipRotation==false) WindDirection(0.0f); 
			//YardsAngle(0.4f);
			//HeadsailsAngle(-0.5f);	
		}
			
		void Update () 
		{
			GunPorts();
			SetSails();
			// New in 1.0.1:
			if(SailClothLOD0[0].damping!=SailClothDamping) ChangeSailDamping();
			//
		}

		// update the compass needle
		void LateUpdate()
		{
			// set adjustedNorth to match it to your prefered north direction
			float adjustedNorth=adjustNorth;
			adjustedNorth=Mathf.Clamp(adjustedNorth, 0.0f, 360.0f)+transform.eulerAngles.y;

			if(adjustedNorth>360.0f)
			{
				adjustedNorth=adjustedNorth-360.0f;
			}
			else
			{
				if(adjustedNorth<0.0f)
				{
					adjustedNorth=adjustedNorth+360.0f;
				}
			}	
			CompassNeedle.localEulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);

			if(windDirectionFollowsShipRotation)
			{
				float wdir = cutterTransform.eulerAngles.y;
				wdir=wdir-35.0f;
				if(wdir<0.0f)
				{
					 wdir=wdir+360.0f;
				}
				else
				{	 
					if(wdir>360.0f)
					{
						 wdir=wdir-360.0f;
					}
				}
				WindDirection(wdir);
			}	

			if(oldUseDeckColliders != useDeckColliders)
			{
				DeckCollidersActive(useDeckColliders);
			}	
		}	

		public void DeckCollidersActive(bool deckColliderActive)
		{
			DeckColliderParent.SetActive(deckColliderActive);
			useDeckColliders=deckColliderActive;
			oldUseDeckColliders=deckColliderActive;
		}	

		public void WindDirection(float windDir)
		{
			windD = Mathf.Clamp(windDir, 0.0f, 360.0f);
			windDirection = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*400.0f, 0.0f, 0.0f);
			windDirectionFlag = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*50.0f, 0.0f, 0.0f);
			randomWindDirection = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*8.0f, windSpeed*15.0f, windSpeed*15.0f);
			randomWindDirectionFlag = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*4.0f, windSpeed*7.0f, windSpeed*7.0f);
			
			if(sailsLOD0_only==false) 
			{
				for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].externalAcceleration = windDirection;
					SailClothLOD1[i].externalAcceleration = windDirection;
					SailClothLOD2[i].externalAcceleration = windDirection;
					SailClothLOD0[i].randomAcceleration = randomWindDirection;
					SailClothLOD1[i].randomAcceleration = randomWindDirection;
					SailClothLOD2[i].randomAcceleration = randomWindDirection;
				}

				FlagClothLOD0.externalAcceleration = windDirectionFlag;
				FlagClothLOD1.externalAcceleration = windDirectionFlag;
				FlagClothLOD2.externalAcceleration = windDirectionFlag;
				FlagClothLOD0.randomAcceleration = randomWindDirectionFlag;
				FlagClothLOD1.randomAcceleration = randomWindDirectionFlag;
				FlagClothLOD2.randomAcceleration = randomWindDirectionFlag;
			}
			else
			{
				for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].externalAcceleration = windDirection;			
					SailClothLOD0[i].randomAcceleration = randomWindDirection;
				}

				FlagClothLOD0.externalAcceleration = windDirectionFlag;
				FlagClothLOD0.randomAcceleration = randomWindDirectionFlag;
			}	
		}	

		public void WindSpeed(float windSpd)
		{
			windSpeed = Mathf.Clamp(windSpd, 0.0f, 1.0f);
			windSpeed=windSpeed*3.0f;
			windDirection = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*400.0f, 0.0f, 0.0f);
			windDirectionFlag = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*50.0f, 0.0f, 0.0f);
			randomWindDirection = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*8.0f, windSpeed*15.0f, windSpeed*15.0f);
			randomWindDirectionFlag = Quaternion.Euler(0.0f, windD, 0.0f) * new Vector3(windSpeed*4.0f, windSpeed*7.0f, windSpeed*7.0f);

			if(sailsLOD0_only==false) 
			{
				for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].externalAcceleration = windDirection;
					SailClothLOD1[i].externalAcceleration = windDirection;
					SailClothLOD2[i].externalAcceleration = windDirection;
					SailClothLOD0[i].randomAcceleration = randomWindDirection;
					SailClothLOD1[i].randomAcceleration = randomWindDirection;
					SailClothLOD2[i].randomAcceleration = randomWindDirection;
				}

				FlagClothLOD0.externalAcceleration = windDirectionFlag;
				FlagClothLOD1.externalAcceleration = windDirectionFlag;
				FlagClothLOD2.externalAcceleration = windDirectionFlag;
				FlagClothLOD0.randomAcceleration = randomWindDirectionFlag;
				FlagClothLOD1.randomAcceleration = randomWindDirectionFlag;
				FlagClothLOD2.randomAcceleration = randomWindDirectionFlag;
			}
			else
			{
				for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].externalAcceleration = windDirection;
					SailClothLOD0[i].randomAcceleration = randomWindDirection;
				}

				FlagClothLOD0.externalAcceleration = windDirectionFlag;
				FlagClothLOD0.randomAcceleration = randomWindDirectionFlag;
			}	
		}	

		void ResetGunPorts()
		{
			openGunPortLid = new bool[14];
			gunPortLidOpened = new bool[14];
			gunPortLidClosed = new bool[14];
			lidOpeningAmount = new float[14];
			upperLidTargetL = new float[7];
			lowerLidTargetL = new float[7];
			upperLidTargetR = new float[7];
			lowerLidTargetR = new float[7];
			
			GunPortTargetsSetup();
			
			LidAngularVelocity = new float[14];
			for(int i = 0; i < 14; i++)
	        {
				LidAngularVelocity[i]=Random.Range(1.25f, 2.5f);

				gunPortUpperLidLOD0[i].localEulerAngles = new Vector3(gunPortUpperLidLOD0[i].localEulerAngles.x, gunPortUpperLidLOD0[i].localEulerAngles.y, 0.0f);
				gunPortLowerLidLOD0[i].localEulerAngles = new Vector3(gunPortLowerLidLOD0[i].localEulerAngles.x, gunPortLowerLidLOD0[i].localEulerAngles.y, 0.0f);
				gunPortUpperLidLOD1[i].localEulerAngles = new Vector3(gunPortUpperLidLOD1[i].localEulerAngles.x, gunPortUpperLidLOD1[i].localEulerAngles.y, 0.0f);
				gunPortLowerLidLOD1[i].localEulerAngles = new Vector3(gunPortLowerLidLOD1[i].localEulerAngles.x, gunPortLowerLidLOD1[i].localEulerAngles.y, 0.0f);
	        }	
		}	

		void ResetSails()
		{
			if(sailsLOD0_only==false) 
			{
				setSail = new bool[9];
				sailSet = new bool[9];
				sailDown = new bool[9];
				sailSetAmount = new float[9];
				sailLOD0 = new GameObject[9];
				sailLOD1 = new GameObject[9];
				sailLOD2 = new GameObject[9];
				sailBSLOD0 = new GameObject[9];
				sailBSLOD1 = new GameObject[9];
				sailBSLOD2 = new GameObject[9];
				
				blendShapeCountLOD0 = new int[9];
		    	skinnedMeshRendererLOD0 = new SkinnedMeshRenderer[9];
		    	skinnedMeshLOD0 = new Mesh[9];
		    	blendShapeCountLOD1 = new int[9];
		    	skinnedMeshRendererLOD1 = new SkinnedMeshRenderer[9];
		    	skinnedMeshLOD1 = new Mesh[9];
		    	blendShapeCountLOD2 = new int[9];
		    	skinnedMeshRendererLOD2 = new SkinnedMeshRenderer[9];
		    	skinnedMeshLOD2 = new Mesh[9];

		    	SailClothLOD0 = new Cloth[9];
		    	SailClothLOD1 = new Cloth[9];
		    	SailClothLOD2 = new Cloth[9];
		    	sailRendererLOD0 = new SkinnedMeshRenderer[9];
		    	sailRendererLOD1 = new SkinnedMeshRenderer[9];
		    	sailRendererLOD2 = new SkinnedMeshRenderer[9];
		    }
		    else
		    {
				setSail = new bool[9];
				sailSet = new bool[9];
				sailDown = new bool[9];
				sailSetAmount = new float[9];
				sailLOD0 = new GameObject[9];				
				sailBSLOD0 = new GameObject[9];
					
				blendShapeCountLOD0 = new int[9];
		    	skinnedMeshRendererLOD0 = new SkinnedMeshRenderer[9];
		    	skinnedMeshLOD0 = new Mesh[9];		    	

		    	SailClothLOD0 = new Cloth[9];
		    	
		    	sailRendererLOD0 = new SkinnedMeshRenderer[9];
		    }	
	    	windBlend = new float[9];

	    	studdingSailYardarmStarboardParent.localPosition = new Vector3(studdingSailYardarmStarboardParentPositionX, studdingSailYardarmStarboardParent.localPosition.y, studdingSailYardarmStarboardParent.localPosition.z);
	    	studdingSailYardarmPortParent.localPosition = new Vector3(studdingSailYardarmPortParentPositionX, studdingSailYardarmPortParent.localPosition.y, studdingSailYardarmPortParent.localPosition.z);

			SailSetup();	

			// Want to change velocity/duration of hissing the sails? Change it here:
			sailSetVelocity = new float[9];
	        sailSetVelocity[0]=2.0f;
	        sailSetVelocity[1]=2.5f;
	        sailSetVelocity[2]=2.5f;
	        sailSetVelocity[3]=2.0f;
	        sailSetVelocity[4]=1.5f;
	        sailSetVelocity[5]=2.0f;
	        sailSetVelocity[6]=3.5f;
	        sailSetVelocity[7]=0.95f;
	        sailSetVelocity[8]=0.95f;
		}


		// new in 1.1:

		public void FireCannonNumber(int i) 
		{
			CannonsFXcontrol.Cannon[i].FireCannon(CannonsFXcontrol.ReloadTime);
		}
		
		public void FireBroadsideStarboard()
		{
			CannonsFXcontrol.FireBroadsideStarboard();
		}
		
		public void FireBroadsidePort()
		{
			CannonsFXcontrol.FireBroadsidePort();
		}	

		// end of new in 1.1


		public void OpenGunPort(int i)
		{
			openGunPortLid[i]=true;
		}	

		public void CloseGunPort(int i)
		{
			openGunPortLid[i]=false;
		}

		public void OpenAllGunPorts()
		{
			OpenStarboardGunPorts();
			OpenPortGunPorts();
		}	

		public void CloseAllGunPorts()
		{
			CloseStarboardGunPorts();
			ClosePortGunPorts();
		}	

		public void OpenStarboardGunPorts()
		{
			for(int i = 0; i < 7; i++)
	        {
	        	openGunPortLid[i]=true;
	        	if(lidOpeningAmount[i]<=0.0f)
	        	{
	        		lidOpeningAmount[i]=Random.Range(-1.0f, 0.0f);
	        	}	
	        }	
		}

		public void OpenPortGunPorts()
		{
			for(int i = 7; i < 14; i++)
	        {
	        	openGunPortLid[i]=true;
	        	if(lidOpeningAmount[i]<=0.0f)
	        	{
	        		lidOpeningAmount[i]=Random.Range(-1.0f, 0.0f);
	        	}	
	        }	
		}

		public void CloseStarboardGunPorts()
		{
			for(int i = 0; i < 7; i++)
	        {
	        	openGunPortLid[i]=false;
	        	if(lidOpeningAmount[i]>=1.0f)
	        	{
	        		lidOpeningAmount[i]=Random.Range(1.0f, 2.0f);
	        	}	
	        }	
		}

		public void ClosePortGunPorts()
		{
			for(int i = 7; i < 14; i++)
	        {
	        	openGunPortLid[i]=false;
	        	if(lidOpeningAmount[i]>=1.0f)
	        	{
	        		lidOpeningAmount[i]=Random.Range(1.0f, 2.0f);
	        	}	
	        }	
		}
			

		void GunPorts()
		{
			float upperLidAngle;
			float lowerLidAngle;

			for(int i = 0; i < 14; i++)
	        {
				if(openGunPortLid[i] && gunPortLidOpened[i]==false)
				{
					gunPortLidClosed[i]=false;

					if(lidOpeningAmount[i]>=1.0f) 
					{
						lidOpeningAmount[i]=1.0f;
						gunPortLidOpened[i]=true;
					}

					if(i<7)
					{
						upperLidAngle=Mathf.Lerp(360.0f, upperLidTargetL[i], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
						lowerLidAngle=Mathf.Lerp(0.0f, lowerLidTargetL[i], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
					}
					else
					{
						upperLidAngle=Mathf.Lerp(0.0f, upperLidTargetR[i-7], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
						lowerLidAngle=Mathf.Lerp(360.0f, lowerLidTargetR[i-7], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
					}

					gunPortUpperLidLOD0[i].localEulerAngles = new Vector3(gunPortUpperLidLOD0[i].localEulerAngles.x, gunPortUpperLidLOD0[i].localEulerAngles.y, upperLidAngle);
					gunPortLowerLidLOD0[i].localEulerAngles = new Vector3(gunPortLowerLidLOD0[i].localEulerAngles.x, gunPortLowerLidLOD0[i].localEulerAngles.y, lowerLidAngle);
					gunPortUpperLidLOD1[i].localEulerAngles = new Vector3(gunPortUpperLidLOD1[i].localEulerAngles.x, gunPortUpperLidLOD1[i].localEulerAngles.y, upperLidAngle);
					gunPortLowerLidLOD1[i].localEulerAngles = new Vector3(gunPortLowerLidLOD1[i].localEulerAngles.x, gunPortLowerLidLOD1[i].localEulerAngles.y, lowerLidAngle);
					
					lidOpeningAmount[i]=lidOpeningAmount[i]+Time.deltaTime*LidAngularVelocity[i];				
				}	
				else
				{
					if(openGunPortLid[i]==false && gunPortLidClosed[i]==false)
					{
						gunPortLidOpened[i]=false;

						if(lidOpeningAmount[i]<=0.0f) 
						{
							lidOpeningAmount[i]=0.0f;
							gunPortLidClosed[i]=true;
						}

						if(i<7)
						{
							upperLidAngle=Mathf.Lerp(360.0f, upperLidTargetL[i], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
							lowerLidAngle=Mathf.Lerp(0.0f, lowerLidTargetL[i], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
						}
						else
						{
							upperLidAngle=Mathf.Lerp(0.0f, upperLidTargetR[i-7], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
							lowerLidAngle=Mathf.Lerp(360.0f, lowerLidTargetR[i-7], Mathf.Clamp(lidOpeningAmount[i], 0.0f,1.0f));
						}

						gunPortUpperLidLOD0[i].localEulerAngles = new Vector3(gunPortUpperLidLOD0[i].localEulerAngles.x, gunPortUpperLidLOD0[i].localEulerAngles.y, upperLidAngle);
						gunPortLowerLidLOD0[i].localEulerAngles = new Vector3(gunPortLowerLidLOD0[i].localEulerAngles.x, gunPortLowerLidLOD0[i].localEulerAngles.y, lowerLidAngle);
						gunPortUpperLidLOD1[i].localEulerAngles = new Vector3(gunPortUpperLidLOD1[i].localEulerAngles.x, gunPortUpperLidLOD1[i].localEulerAngles.y, upperLidAngle);
						gunPortLowerLidLOD1[i].localEulerAngles = new Vector3(gunPortLowerLidLOD1[i].localEulerAngles.x, gunPortLowerLidLOD1[i].localEulerAngles.y, lowerLidAngle);

						lidOpeningAmount[i]=lidOpeningAmount[i]-Time.deltaTime*LidAngularVelocity[i];
					}	
				}	
			}	
		}	

		void GunPortTargetsSetup()
		{
			upperLidTargetL[0] = 270.0f;
			lowerLidTargetL[0] = 95.0f;
			upperLidTargetL[1] = 270.0f;
			lowerLidTargetL[1] = 150.0f;
			upperLidTargetL[2] = 270.0f;
			lowerLidTargetL[2] = 100.0f;
			upperLidTargetL[3] = 270.0f;
			lowerLidTargetL[3] = 100.0f;
			upperLidTargetL[4] = 270.0f;
			lowerLidTargetL[4] = 150.0f;
			upperLidTargetL[5] = 270.0f;
			lowerLidTargetL[5] = 150.0f;
			upperLidTargetL[6] = 270.0f;
			lowerLidTargetL[6] = 150.0f;

			upperLidTargetR[0] = 90.0f;
			lowerLidTargetR[0] = 265.0f;
			upperLidTargetR[1] = 90.0f;
			lowerLidTargetR[1] = 210.0f;
			upperLidTargetR[2] = 90.0f;
			lowerLidTargetR[2] = 260.0f;
			upperLidTargetR[3] = 90.0f;
			lowerLidTargetR[3] = 260.0f;
			upperLidTargetR[4] = 90.0f;
			lowerLidTargetR[4] = 210.0f;
			upperLidTargetR[5] = 90.0f;
			lowerLidTargetR[5] = 210.0f;
			upperLidTargetR[6] = 90.0f;
			lowerLidTargetR[6] = 210.0f;
		}
			
		
		public void AllBarrelAngles(float barrelAngle)
		{
			for(int i=0;i<14;i++)
			{
				BarrelAngle(i, barrelAngle);
			}	
		}	

		public void AllBarrelAnglesPort(float barrelAngle)
		{
			for(int i=0;i<7;i++)
			{
				BarrelAngle(i, barrelAngle);
			}	
		}	

		public void AllBarrelAnglesStarboard(float barrelAngle)
		{
			for(int i=7;i<14;i++)
			{
				BarrelAngle(i, barrelAngle);
			}	
		}	

		public void BarrelAngle(int barrel, float barrelAngle)
		{
			// Set angular limits here:
			float barrelLowerLimit = 2.661f;
			float barrelUpperLimit = -10.874f;
			float angle;
			if(barrelAngle<0.0)
			{
				angle = Mathf.Lerp(barrelLowerLimit, 0.0f, barrelAngle+1.0f);
			}
			else
			{
				angle = Mathf.Lerp(0.0f, barrelUpperLimit, barrelAngle);
			}	
			
			if(angle>=0)
			{
				carronadeVertical[barrel].localEulerAngles =  new Vector3(0.0f, 0.0f, angle);
			}
			else
			{
				carronadeVertical[barrel].localEulerAngles =  new Vector3(0.0f, 0.0f, 360.0f+angle);
			}	
		}

		public void AllPedestalAngles(float pedestalAngle)
		{
			for(int i=0;i<14;i++)
			{
				PedestalAngle(i, pedestalAngle);
			}	
		}	

		public void AllPedestalAnglesPort(float pedestalAngle)
		{
			for(int i=0;i<7;i++)
			{
				PedestalAngle(i, pedestalAngle);
			}	
		}	

		public void AllPedestalAnglesStarboard(float pedestalAngle)
		{
			for(int i=7;i<14;i++)
			{
				PedestalAngle(i, pedestalAngle);
			}	
		}	

		public void PedestalAngle(int pedestal, float pedestalAngle)
		{
			// Set angular limits here:
			float pedestalLimits = 20.0f;	

			if(pedestalAngle>=0)
			{
				carronadeHorizontal[pedestal].localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(0.0f, pedestalLimits, pedestalAngle), 0.0f);
			}
			else
			{
				carronadeHorizontal[pedestal].localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(360.0f-pedestalLimits, 360.0f, 1.0f+pedestalAngle), 0.0f);
			}	
		}

		public void AllCarriagePositions(float carriagePosition)
		{
			for(int i=0;i<14;i++)
			{
				CarriagePosition(i, carriagePosition);
			}	
		}	

		public void AllCarriagePositionsPort(float carriagePosition)
		{
			for(int i=0;i<7;i++)
			{
				CarriagePosition(i, carriagePosition);
			}	
		}	

		public void AllCarriagePositionsStarboard(float carriagePosition)
		{
			for(int i=7;i<14;i++)
			{
				CarriagePosition(i, carriagePosition);
			}	
		}	

		public void CarriagePosition(int pedestal, float carriagePosition)
		{
			// Set limit here:
			float carriageLimit = 0.25f;			
		 	carronadeSlider[pedestal].localPosition = new Vector3(Mathf.Lerp(0.0f, carriageLimit, carriagePosition), carronadeSlider[pedestal].localPosition.y, carronadeSlider[pedestal].localPosition.z);
		}

		public void RudderAngle(float rudderAngle)
		{
			// Set angular limits here:
			float rudderLimits = 35.0f;	
			float rudderMasterAngle;

			if(rudderAngle>=0)
			{
				rudderMasterAngle =  Mathf.Lerp(0.0f, rudderLimits, rudderAngle);
				rudderLever.localEulerAngles = new Vector3(0.0f, rudderMasterAngle*0.45774285714286f, 0.0f);
				rudderRopeBlendShape.SetBlendShapeWeight(1, rudderAngle*100.0f);
			}
			else
			{
				rudderMasterAngle =  Mathf.Lerp(360.0f-rudderLimits, 360.0f, 1.0f+rudderAngle);
				rudderLever.localEulerAngles = new Vector3(0.0f, Mathf.Lerp(360.0f-rudderLimits*0.45774285714286f, 360.0f, 1.0f+rudderAngle), 0.0f);
				rudderRopeBlendShape.SetBlendShapeWeight(0, Mathf.Abs(rudderAngle)*100.0f);
			}	

			rudder.localEulerAngles = new Vector3(0.0f, rudderMasterAngle, 0.0f);		
			rudderWheelParent.localEulerAngles = new Vector3(0.0f, 0.0f, rudderMasterAngle*5.0f);

			rudderRopeBlendShape.SetBlendShapeWeight(1, rudderAngle*100.0f);
		}

		public void YardsAngle(float yardarmAngle)
		{
			// Set angular limits here:
			float yardarmLimits = 45.0f;

			if(yardarmAngle>=0)
			{
				yardarmParentRotation.localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(0.0f, yardarmLimits, yardarmAngle), 0.0f);
			}
			else
			{
				yardarmParentRotation.localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(360.0f-yardarmLimits, 360.0f, 1.0f+yardarmAngle), 0.0f);
			}	
		}

		public void BoomAngle(float rearYardsAngle)
		{
			// Set angular limits here:
			float rearYardarmLimits = 45.0f;

			if(rearYardsAngle>=0)
			{
				rearYardarmParentRotation.localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(0.0f, rearYardarmLimits, rearYardsAngle), 0.0f);
			}
			else
			{
				rearYardarmParentRotation.localEulerAngles =  new Vector3(0.0f, Mathf.Lerp(360.0f-rearYardarmLimits, 360.0f, 1.0f+rearYardsAngle), 0.0f);
			}	
		}

		public void HeadsailsAngle(float headsailsAngle)
		{
			// Set angular limits here:
			float frontSailsLimits = 45.0f;

			if(headsailsAngle>=0)
			{
				frontSailMasterAngle = new Vector3(0.0f, Mathf.Lerp(0.0f, frontSailsLimits, headsailsAngle), 0.0f);
			}
			else
			{
				frontSailMasterAngle = new Vector3(0.0f, Mathf.Lerp(360.0f-frontSailsLimits, 360.0f, 1.0f+headsailsAngle), 0.0f);
			}	

			if(frontSailMasterAngle.y>180.0f)
			{
				Retracted = new Vector3(0.0f, 360.0f, 0.0f);
			}
			else
			{
				Retracted = new Vector3(0.0f, 0.0f, 0.0f);
			}	

			focParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, sailSetAmount[0]);
			focEnLAirParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, sailSetAmount[1]);
			trinquetteParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, sailSetAmount[2]);		
		}

		public void SetJib(bool setIt)
		{
			SetOrUnsetSails(0, setIt);
		}	

		public void SetJibTopsail(bool setIt)
		{
			SetOrUnsetSails(1, setIt);
		}	

		public void SetStaysail(bool setIt)
		{
			SetOrUnsetSails(2, setIt);
		}	

		public void SetMainsail(bool setIt)
		{
			SetOrUnsetSails(3, setIt);
		}	

		public void SetSquareSail(bool setIt)
		{
			SetOrUnsetSails(4, setIt);
		}	

		public void SetTopSail(bool setIt)
		{
			SetOrUnsetSails(5, setIt);
		}	

		public void SetTopgallantSail(bool setIt)
		{
			SetOrUnsetSails(6, setIt);
		}	

		public void SetStuddingSailPort(bool setIt)
		{
			SetOrUnsetSails(7, setIt);
		}	

		public void SetStuddingSailStarboard(bool setIt)
		{
			SetOrUnsetSails(8, setIt);
		}	

		void SetOrUnsetSails(int i, bool setIt)
		{
			if(setIt && sailDown[i])
			{
				setSail[i]=true;
			}
			else
			{	
				if(setIt==false && sailSet[i])
				{
					windBlend[i]=1.0f;
					StartCoroutine("BlendWindOut", i);
				}	
			}	
		}	

		// to prepare a simple smooth transition between bendshapes and cloth
		IEnumerator BlendWindIn(int i) 
		{
			FadeOut(i);

			while(windBlend[i]<=1.0f)
			{
				// obsolete - exchanged to FadeOut();
				/*
				SailClothLOD0[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD1[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD2[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD0[i].randomAcceleration = randomWindDirection*windBlend[i];
				SailClothLOD1[i].randomAcceleration = randomWindDirection*windBlend[i];
				SailClothLOD2[i].randomAcceleration = randomWindDirection*windBlend[i];
				*/
		    	windBlend[i]=windBlend[i]+Time.deltaTime*1.5f;
		       
		        yield return null;
		    }  

		}

		// to prepare a simple smooth transition between cloth and bendshapes
		IEnumerator BlendWindOut(int i) 
		{
			FadeIn(i);
	
			while(windBlend[i]>=0.0f)
			{
				// obsolete - exchanged with FadeIn();
				/*  
				SailClothLOD0[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD1[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD2[i].externalAcceleration = windDirection*windBlend[i];
				SailClothLOD0[i].randomAcceleration = randomWindDirection*windBlend[i];
				SailClothLOD1[i].randomAcceleration = randomWindDirection*windBlend[i];
				SailClothLOD2[i].randomAcceleration = randomWindDirection*windBlend[i];
				*/
		    	windBlend[i]=windBlend[i]-Time.deltaTime*1.5f;
		      
		        yield return null;
		    }  
		   
		    SwitchToBlendShapes(i);
		   	setSail[i]=false;		 
		}


		void SwitchToBlendShapes(int i)
		{
			// deactivating/activating a cloth renderer/GameObject creates some weird flickering of a cloth renderer which was enabled/disabled BEFORE - bug? – deactivating game object instead
			if(sailsLOD0_only==false) 
			{
				sailRendererLOD0[i].enabled=false;
				sailRendererLOD1[i].enabled=false;
				sailRendererLOD2[i].enabled=false;
				
				SailClothLOD0[i].enabled=false;
				SailClothLOD1[i].enabled=false;
				SailClothLOD2[i].enabled=false;

				sailLOD0[i].SetActive(false);
				sailLOD1[i].SetActive(false);
				sailLOD2[i].SetActive(false);

				skinnedMeshRendererLOD0[i].enabled=true;
				skinnedMeshRendererLOD1[i].enabled=true;
				skinnedMeshRendererLOD2[i].enabled=true;

				sailBSLOD0[i].SetActive(true);
				sailBSLOD1[i].SetActive(true);
				sailBSLOD2[i].SetActive(true);	
			}
			else
			{
				sailRendererLOD0[i].enabled=false;
				SailClothLOD0[i].enabled=false;
				sailLOD0[i].SetActive(false);
				skinnedMeshRendererLOD0[i].enabled=true;
				sailBSLOD0[i].SetActive(true);
			}
		}	

		void SwitchToClothSails(int i)
		{
			// deactivating/activating a cloth renderer/GameObject or object creates some weird flickering of a cloth renderer which was enabled/disabled BEFORE - bug?
			if(sailsLOD0_only==false) 
			{
				sailRendererLOD0[i].enabled=true;
				sailRendererLOD1[i].enabled=true;
				sailRendererLOD2[i].enabled=true;
				
				sailLOD0[i].SetActive(true);
				sailLOD1[i].SetActive(true);
				sailLOD2[i].SetActive(true);

				SailClothLOD0[i].enabled=true;
				SailClothLOD1[i].enabled=true;
				SailClothLOD2[i].enabled=true;			

				skinnedMeshRendererLOD0[i].enabled=false;
				skinnedMeshRendererLOD1[i].enabled=false;
				skinnedMeshRendererLOD2[i].enabled=false;

				sailBSLOD0[i].SetActive(false);
				sailBSLOD1[i].SetActive(false);
				sailBSLOD2[i].SetActive(false);
			}
			else
			{
				sailRendererLOD0[i].enabled=true;		
				sailLOD0[i].SetActive(true);
				SailClothLOD0[i].enabled=true;		
				skinnedMeshRendererLOD0[i].enabled=false;
				sailBSLOD0[i].SetActive(false);
			}	
		}	
		
		// new in 1.0.1
		void ChangeSailDamping()
		{
			SailClothDamping=Mathf.Clamp(SailClothDamping, 0.0f, 1.0f);
			for(int i = 0; i < 9; i++)
	        {
	        	SailClothLOD0[i].damping=SailClothDamping;
	        	if(sailsLOD0_only==false)
	        	{
	        		SailClothLOD1[i].damping=SailClothDamping;
	        		SailClothLOD2[i].damping=SailClothDamping;
	        	}	
	        }	
		}	

		void SetSails()
		{
			float blendAmount;
			float yardArmAmount;

			for(int i = 0; i < 9; i++)
	        {
	
				if(setSail[i] && sailSet[i]==false)
				{
					sailDown[i]=false;

					if(sailSetAmount[i]>=1.0f) 
					{
						sailSetAmount[i]=1.0f;
						sailSet[i]=true;
						SwitchToClothSails(i);
						windBlend[i]=0.0f;
						StartCoroutine("BlendWindIn", i);
					}
					
					if(i==7 || i==8)
					{
						yardArmAmount = (1.0f-Mathf.Clamp(sailSetAmount[i]*2.0f, 0.0f, 1.0f));
						blendAmount = Mathf.Clamp(-1.0f+sailSetAmount[i]*2.0f, 0.0f, 1.0f)*100.0f;
						if(i==7) studdingSailYardarmPortParent.localPosition = new Vector3(studdingSailYardarmPortParentPositionX+(2.6f*yardArmAmount), studdingSailYardarmPortParent.localPosition.y, studdingSailYardarmPortParent.localPosition.z);
						if(i==8) studdingSailYardarmStarboardParent.localPosition = new Vector3(studdingSailYardarmStarboardParentPositionX-(2.6f*yardArmAmount), studdingSailYardarmStarboardParent.localPosition.y, studdingSailYardarmStarboardParent.localPosition.z);
					}
					else
					{
						blendAmount = Mathf.Clamp(sailSetAmount[i], 0.0f, 1.0f)*100.0f;
	 				}

	 				if(sailsLOD0_only==false) 
					{
		 				if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, blendAmount);
		             	if (blendShapeCountLOD1[i] > 0) skinnedMeshRendererLOD1[i].SetBlendShapeWeight (0, blendAmount);
		             	if (blendShapeCountLOD2[i] > 0) skinnedMeshRendererLOD2[i].SetBlendShapeWeight (0, blendAmount);
	 				}
	 				else
	 				{
	 					if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, blendAmount);
	 				}	

	 				if(i==0 || i==1 || i==2 || i==4) MoveRope(i, blendAmount);
	 				if(i==1) MoveJibTopsail(sailSetAmount[i]);
					if(i==0) MoveJib(sailSetAmount[i]);
					if(i==2) MoveStaysail(sailSetAmount[i]);				

					sailSetAmount[i]=sailSetAmount[i]+Time.deltaTime*sailSetVelocity[i]*Mathf.Abs(SailAnimationVelocity);				
				}	
				else
				{
					if(setSail[i]==false && sailDown[i]==false)
					{
						sailSet[i]=false;

						if(sailSetAmount[i]<=0.0f) 
						{
							sailSetAmount[i]=0.0f;
							sailDown[i]=true;
						}
						
						if(i==7 || i==8)
						{
							yardArmAmount = (1.0f-Mathf.Clamp(sailSetAmount[i]*2.0f, 0.0f, 1.0f));
							blendAmount = Mathf.Clamp(-1.0f+sailSetAmount[i]*2.0f, 0.0f, 1.0f)*100.0f;
							if(i==7) studdingSailYardarmPortParent.localPosition = new Vector3(studdingSailYardarmPortParentPositionX+(2.6f*yardArmAmount), studdingSailYardarmPortParent.localPosition.y, studdingSailYardarmPortParent.localPosition.z);
							if(i==8) studdingSailYardarmStarboardParent.localPosition = new Vector3(studdingSailYardarmStarboardParentPositionX-(2.6f*yardArmAmount), studdingSailYardarmStarboardParent.localPosition.y, studdingSailYardarmStarboardParent.localPosition.z);					
						}
						else
						{
							blendAmount = Mathf.Clamp(sailSetAmount[i], 0.0f, 1.0f)*100.0f;
						}

						if(sailsLOD0_only==false) 
						{
		 					if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, blendAmount);
		             		if (blendShapeCountLOD1[i] > 0) skinnedMeshRendererLOD1[i].SetBlendShapeWeight (0, blendAmount);
		             		if (blendShapeCountLOD2[i] > 0) skinnedMeshRendererLOD2[i].SetBlendShapeWeight (0, blendAmount);
		             	}
		             	else
		             	{
		             		if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, blendAmount);
		             	}	
						
						if(i==0 || i==1 || i==2 || i==4) MoveRope(i, sailSetAmount[i]);
						if(i==1) MoveJibTopsail(sailSetAmount[i]);
						if(i==0) MoveJib(sailSetAmount[i]);
						if(i==2) MoveStaysail(sailSetAmount[i]);

						sailSetAmount[i]=sailSetAmount[i]-Time.deltaTime*sailSetVelocity[i]*Mathf.Abs(SailAnimationVelocity);
					}	
				}	
			}	
		}	

		void MoveJibTopsail(float amount)
		{
			Vector3 lerpPos = Vector3.Lerp(loweringEndPosition, raisingEndPosition, amount);	

			if(sailsLOD0_only==false) 
			{
				focEnLAirLOD0.localPosition=lerpPos;
				focEnLAirBlendShapesLOD0.localPosition=lerpPos;
				focEnLAirLOD1.localPosition=lerpPos;
				focEnLAirBlendShapesLOD1.localPosition=lerpPos;
				focEnLAirLOD2.localPosition=lerpPos;
				focEnLAirBlendShapesLOD2.localPosition=lerpPos;
			}
			else
			{
				focEnLAirLOD0.localPosition=lerpPos;
				focEnLAirBlendShapesLOD0.localPosition=lerpPos;
			}	

			Rigging.RopeEndPosition[2].localPosition=Vector3.Lerp(loweringRope2Position, raisingRope2Position, amount);

			if(frontSailMasterAngle.y>180.0f)
			{
				Retracted = new Vector3(0.0f, 360.0f, 0.0f);
			}
			else
			{
				Retracted = new Vector3(0.0f, 0.0f, 0.0f);
			}	
			focEnLAirParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, amount);
		}	

		void MoveJib(float amount)
		{
			Rigging.RopeEndPosition[0].localPosition=Vector3.Lerp(loweringRope0Position, raisingRope0Position, amount);

			if(frontSailMasterAngle.y>180.0f)
			{
				Retracted = new Vector3(0.0f, 360.0f, 0.0f);
			}
			else
			{
				Retracted = new Vector3(0.0f, 0.0f, 0.0f);
			}	
			focParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, amount);
		}	

		void MoveStaysail(float amount)
		{
			if(frontSailMasterAngle.y>180.0f)
			{
				Retracted = new Vector3(0.0f, 360.0f, 0.0f);
			}
			else
			{
				Retracted = new Vector3(0.0f, 0.0f, 0.0f);
			}	
			trinquetteParentRotation.localEulerAngles = Vector3.Lerp(Retracted, frontSailMasterAngle, amount);
			if(amount>0.0f)
			{
				Rigging.RenderRope[1]=true;
			}	
			else
			{
				Rigging.RenderRope[1]=false;
			}	
		}

		void MoveRope(int i, float amount)
		{
			if(i==0) 
			{
				Rigging.RopeEndPosition[13].localPosition=Vector3.Lerp(RopeEndPosB, RopeEndPosA,amount);
			}
			else
			{
				if(i==1) 
				{
					Rigging.RopeEndPosition[13].localPosition=Vector3.Lerp(RopeEndPos2B, RopeEndPos2A, amount);
				}
				else
				{	
					if(i==2) 
					{
						Rigging.RopeEndPosition[13].localPosition=Vector3.Lerp(RopeEndPos1B, RopeEndPos1A, amount);
					}	
					else
					{
						if(i==4) 
						{
							Rigging.RopeEndPosition[13].localPosition=Vector3.Lerp(RopeEndPos13B, RopeEndPos13A, amount);
							Rigging.RopeEndPosition[14].localPosition=Vector3.Lerp(RopeEndPos14B, RopeEndPos14A, amount);
						}
					}
				}
			}			
		}	

		void SailSetup()
		{
			if(sailsLOD0_only==false) 
			{
				sailLOD0[0]=focLOD0.gameObject;
				sailLOD1[0]=focLOD1.gameObject;
				sailLOD2[0]=focLOD2.gameObject;

				sailBSLOD0[0]=focBlendShapesLOD0.gameObject;
				sailBSLOD1[0]=focBlendShapesLOD1.gameObject;
				sailBSLOD2[0]=focBlendShapesLOD2.gameObject;

				sailLOD0[1]=focEnLAirLOD0.gameObject;
				sailLOD1[1]=focEnLAirLOD1.gameObject;
				sailLOD2[1]=focEnLAirLOD2.gameObject;
				sailBSLOD0[1]=focEnLAirBlendShapesLOD0.gameObject;
				sailBSLOD1[1]=focEnLAirBlendShapesLOD1.gameObject;
				sailBSLOD2[1]=focEnLAirBlendShapesLOD2.gameObject;

				sailLOD0[2]=trinquetteLOD0.gameObject;
				sailLOD1[2]=trinquetteLOD1.gameObject;
				sailLOD2[2]=trinquetteLOD2.gameObject;
				sailBSLOD0[2]=trinquetteBlendShapesLOD0.gameObject;
				sailBSLOD1[2]=trinquetteBlendShapesLOD1.gameObject;
				sailBSLOD2[2]=trinquetteBlendShapesLOD2.gameObject;

				sailLOD0[3]=grandVoileLOD0.gameObject;
				sailLOD1[3]=grandVoileLOD1.gameObject;
				sailLOD2[3]=grandVoileLOD2.gameObject;
				sailBSLOD0[3]=grandVoileBlendShapesLOD0.gameObject;
				sailBSLOD1[3]=grandVoileBlendShapesLOD1.gameObject;
				sailBSLOD2[3]=grandVoileBlendShapesLOD2.gameObject;

				sailLOD0[4]=fortuneLOD0.gameObject;
				sailLOD1[4]=fortuneLOD1.gameObject;
				sailLOD2[4]=fortuneLOD2.gameObject;
				sailBSLOD0[4]=fortuneBlendShapesLOD0.gameObject;
				sailBSLOD1[4]=fortuneBlendShapesLOD1.gameObject;
				sailBSLOD2[4]=fortuneBlendShapesLOD2.gameObject;

				sailLOD0[5]=hunierLOD0.gameObject;
				sailLOD1[5]=hunierLOD1.gameObject;
				sailLOD2[5]=hunierLOD2.gameObject;
				sailBSLOD0[5]=hunierBlendShapesLOD0.gameObject;
				sailBSLOD1[5]=hunierBlendShapesLOD1.gameObject;
				sailBSLOD2[5]=hunierBlendShapesLOD2.gameObject;

				sailLOD0[6]=perroquetLOD0.gameObject;
				sailLOD1[6]=perroquetLOD1.gameObject;
				sailLOD2[6]=perroquetLOD2.gameObject;
				sailBSLOD0[6]=perroquetBlendShapesLOD0.gameObject;
				sailBSLOD1[6]=perroquetBlendShapesLOD1.gameObject;
				sailBSLOD2[6]=perroquetBlendShapesLOD2.gameObject;

				sailLOD0[7]=studdingSailPortLOD0.gameObject;
				sailLOD1[7]=studdingSailPortLOD1.gameObject;
				sailLOD2[7]=studdingSailPortLOD2.gameObject;
				sailBSLOD0[7]=studdingSailPortBlendShapesLOD0.gameObject;
				sailBSLOD1[7]=studdingSailPortBlendShapesLOD1.gameObject;
				sailBSLOD2[7]=studdingSailPortBlendShapesLOD2.gameObject;

				sailLOD0[8]=studdingSailStarboardLOD0.gameObject;
				sailLOD1[8]=studdingSailStarboardLOD1.gameObject;
				sailLOD2[8]=studdingSailStarboardLOD2.gameObject;
				sailBSLOD0[8]=studdingSailStarboardBlendShapesLOD0.gameObject;
				sailBSLOD1[8]=studdingSailStarboardBlendShapesLOD1.gameObject;
				sailBSLOD2[8]=studdingSailStarboardBlendShapesLOD2.gameObject;

				for(int i = 0; i < 9; i++)
		        {
					sailLOD0[i].SetActive(true);		
					sailLOD1[i].SetActive(true);
					sailLOD2[i].SetActive(true);
					sailBSLOD0[i].SetActive(false);
					sailBSLOD1[i].SetActive(false);
					sailBSLOD2[i].SetActive(false);
					
					if(startWithSailsSet)
					{
						setSail[i]=true;
						sailSetAmount[i]=1.0f;
						sailSet[i]=true;
						sailDown[i]=false;
					}
					else
					{
						setSail[i]=false;
						sailSetAmount[i]=0.0f;
						sailSet[i]=false;
						sailDown[i]=true;
					}

					skinnedMeshRendererLOD0[i] = sailBSLOD0[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
					skinnedMeshRendererLOD1[i] = sailBSLOD1[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
					skinnedMeshRendererLOD2[i] = sailBSLOD2[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
		            skinnedMeshLOD0[i] = skinnedMeshRendererLOD0[i].sharedMesh;
		            skinnedMeshLOD1[i] = skinnedMeshRendererLOD1[i].sharedMesh;
		            skinnedMeshLOD2[i] = skinnedMeshRendererLOD2[i].sharedMesh;
		            blendShapeCountLOD0[i] = skinnedMeshLOD0[i].blendShapeCount;
		            blendShapeCountLOD1[i] = skinnedMeshLOD1[i].blendShapeCount;
		            blendShapeCountLOD2[i] = skinnedMeshLOD2[i].blendShapeCount;

		            if(startWithSailsSet)
					{
			            if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, 100.0f);
			            if (blendShapeCountLOD1[i] > 0) skinnedMeshRendererLOD1[i].SetBlendShapeWeight (0, 100.0f);
			            if (blendShapeCountLOD2[i] > 0) skinnedMeshRendererLOD2[i].SetBlendShapeWeight (0, 100.0f);
			        }
			        else
			        {
			        	if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, 0.0f);
			            if (blendShapeCountLOD1[i] > 0) skinnedMeshRendererLOD1[i].SetBlendShapeWeight (0, 0.0f);
			            if (blendShapeCountLOD2[i] > 0) skinnedMeshRendererLOD2[i].SetBlendShapeWeight (0, 0.0f);
			        }
			        	
		            SailClothLOD0[i]=sailLOD0[i].GetComponent<Cloth>();
		    		SailClothLOD1[i]=sailLOD1[i].GetComponent<Cloth>();
		    		SailClothLOD2[i]=sailLOD2[i].GetComponent<Cloth>();

		    		sailRendererLOD0[i] = sailLOD0[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
		    		sailRendererLOD1[i] = sailLOD1[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
		    		sailRendererLOD2[i] = sailLOD2[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;

		    		sailLOD0[i].SetActive(true);
					sailLOD1[i].SetActive(true);
					sailLOD2[i].SetActive(true);
					sailBSLOD0[i].SetActive(true);
					sailBSLOD1[i].SetActive(true);
					sailBSLOD2[i].SetActive(true);

					if(startWithSailsSet)
					{
						SwitchToClothSails(i);
						if(i==0 || i==1 || i==2 || i==4) MoveRope(i, 1.0f);
					}
					else
					{
						SwitchToBlendShapes(i);
						if(i==7) studdingSailYardarmPortParent.localPosition = new Vector3(studdingSailYardarmPortParentPositionX+(2.6f), studdingSailYardarmPortParent.localPosition.y, studdingSailYardarmPortParent.localPosition.z);
						if(i==8) studdingSailYardarmStarboardParent.localPosition = new Vector3(studdingSailYardarmStarboardParentPositionX-(2.6f), studdingSailYardarmStarboardParent.localPosition.y, studdingSailYardarmStarboardParent.localPosition.z);					
						if(i==0 || i==1 || i==2 || i==4) MoveRope(i, 0.0f);
						if(i==1) MoveJibTopsail(sailSetAmount[i]);
						if(i==0) MoveJib(sailSetAmount[i]);
						if(i==2) MoveStaysail(sailSetAmount[i]);
					}	
		        } 

		        FlagClothLOD0=FlagLOD0.GetComponent<Cloth>();
		    	FlagClothLOD1=FlagLOD1.GetComponent<Cloth>();
		    	FlagClothLOD2=FlagLOD2.GetComponent<Cloth>();
		    }
		    else
		    {
		    	sailLOD0[0]=focLOD0.gameObject;
				sailBSLOD0[0]=focBlendShapesLOD0.gameObject;

				sailLOD0[1]=focEnLAirLOD0.gameObject;
				sailBSLOD0[1]=focEnLAirBlendShapesLOD0.gameObject;

				sailLOD0[2]=trinquetteLOD0.gameObject;
				sailBSLOD0[2]=trinquetteBlendShapesLOD0.gameObject;

				sailLOD0[3]=grandVoileLOD0.gameObject;
				sailBSLOD0[3]=grandVoileBlendShapesLOD0.gameObject;

				sailLOD0[4]=fortuneLOD0.gameObject;
				sailBSLOD0[4]=fortuneBlendShapesLOD0.gameObject;

				sailLOD0[5]=hunierLOD0.gameObject;
				sailBSLOD0[5]=hunierBlendShapesLOD0.gameObject;

				sailLOD0[6]=perroquetLOD0.gameObject;
				sailBSLOD0[6]=perroquetBlendShapesLOD0.gameObject;

				sailLOD0[7]=studdingSailPortLOD0.gameObject;
				sailBSLOD0[7]=studdingSailPortBlendShapesLOD0.gameObject;

				sailLOD0[8]=studdingSailStarboardLOD0.gameObject;
				sailBSLOD0[8]=studdingSailStarboardBlendShapesLOD0.gameObject;


				for(int i = 0; i < 9; i++)
		        {
					sailLOD0[i].SetActive(true);		
					sailBSLOD0[i].SetActive(false);
				
					if(startWithSailsSet)
					{
						setSail[i]=true;
						sailSetAmount[i]=1.0f;
						sailSet[i]=true;
						sailDown[i]=false;
					}
					else
					{
						setSail[i]=false;
						sailSetAmount[i]=0.0f;
						sailSet[i]=false;
						sailDown[i]=true;
					}

					skinnedMeshRendererLOD0[i] = sailBSLOD0[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;
		            skinnedMeshLOD0[i] = skinnedMeshRendererLOD0[i].sharedMesh;
		            blendShapeCountLOD0[i] = skinnedMeshLOD0[i].blendShapeCount;

		            if(startWithSailsSet)
					{
			            if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, 100.0f);
			        }
			        else
			        {
			        	if (blendShapeCountLOD0[i] > 0) skinnedMeshRendererLOD0[i].SetBlendShapeWeight (0, 0.0f);
			        }
			        	
		            SailClothLOD0[i]=sailLOD0[i].GetComponent<Cloth>();

		    		sailRendererLOD0[i] = sailLOD0[i].GetComponent( typeof(SkinnedMeshRenderer) ) as SkinnedMeshRenderer;

		    		sailLOD0[i].SetActive(true);
					sailBSLOD0[i].SetActive(true);


					if(startWithSailsSet)
					{
						SwitchToClothSails(i);
						if(i==0 || i==1 || i==2 || i==4) MoveRope(i, 1.0f);
					}
					else
					{
						SwitchToBlendShapes(i);
						if(i==7) studdingSailYardarmPortParent.localPosition = new Vector3(studdingSailYardarmPortParentPositionX+(2.6f), studdingSailYardarmPortParent.localPosition.y, studdingSailYardarmPortParent.localPosition.z);
						if(i==8) studdingSailYardarmStarboardParent.localPosition = new Vector3(studdingSailYardarmStarboardParentPositionX-(2.6f), studdingSailYardarmStarboardParent.localPosition.y, studdingSailYardarmStarboardParent.localPosition.z);					
						if(i==0 || i==1 || i==2 || i==4) MoveRope(i, 0.0f);
						if(i==1) MoveJibTopsail(sailSetAmount[i]);
						if(i==0) MoveJib(sailSetAmount[i]);
						if(i==2) MoveStaysail(sailSetAmount[i]);
					}	
		        } 

		        FlagClothLOD0=FlagLOD0.GetComponent<Cloth>();
		    }			
		}	


		void ReparentSails()
		{
			if(sailsLOD0_only==false) 
			{
				yardarmLOD0.parent = yardarmParentRotation;
				yardarmLOD1.parent = yardarmParentRotation;
				yardarmLOD2.parent = yardarmParentRotation;

				yardarmBlendshapesLOD0.parent = yardarmParentRotation;
				yardarmBlendshapesLOD1.parent = yardarmParentRotation;
				yardarmBlendshapesLOD2.parent = yardarmParentRotation;

				rearYardarmLOD0.parent = rearYardarmParentRotation;
				rearYardarmLOD1.parent = rearYardarmParentRotation;
				rearYardarmLOD2.parent = rearYardarmParentRotation;

				rearYardarmBlendshapesLOD0.parent = rearYardarmParentRotation;
				rearYardarmBlendshapesLOD1.parent = rearYardarmParentRotation;
				rearYardarmBlendshapesLOD2.parent = rearYardarmParentRotation;
				
				focLOD0.parent = focParentRotation;
				focEnLAirLOD0.parent = focEnLAirParentRotation;
				trinquetteLOD0.parent = trinquetteParentRotation;
				
				focLOD1.parent = focParentRotation;
				focEnLAirLOD1.parent = focEnLAirParentRotation;
				trinquetteLOD1.parent = trinquetteParentRotation;
				
				focLOD2.parent = focParentRotation;
				focEnLAirLOD2.parent = focEnLAirParentRotation;
				trinquetteLOD2.parent = trinquetteParentRotation;
				
				focBlendShapesLOD0.parent = focParentRotation;
				focEnLAirBlendShapesLOD0.parent = focEnLAirParentRotation;
				trinquetteBlendShapesLOD0.parent = trinquetteParentRotation;
				
				focBlendShapesLOD1.parent = focParentRotation;
				focEnLAirBlendShapesLOD1.parent = focEnLAirParentRotation;
				trinquetteBlendShapesLOD1.parent = trinquetteParentRotation;

				focBlendShapesLOD2.parent = focParentRotation;
				focEnLAirBlendShapesLOD2.parent = focEnLAirParentRotation;
				trinquetteBlendShapesLOD2.parent = trinquetteParentRotation;

				studdingYardarmPortLOD0.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD0.parent = studdingSailYardarmStarboardParent;
				studdingYardarmPortLOD1.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD1.parent = studdingSailYardarmStarboardParent;
				studdingYardarmPortLOD2.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD2.parent = studdingSailYardarmStarboardParent;

				studdingSailPortBlendShapesLOD0.parent = studdingSailYardarmPortParent;
				studdingSailStarboardBlendShapesLOD0.parent = studdingSailYardarmStarboardParent;

				studdingSailPortBlendShapesLOD1.parent = studdingSailYardarmPortParent;
				studdingSailStarboardBlendShapesLOD1.parent = studdingSailYardarmStarboardParent;

				studdingSailPortBlendShapesLOD2.parent = studdingSailYardarmPortParent;
				studdingSailStarboardBlendShapesLOD2.parent = studdingSailYardarmStarboardParent;

		    	studdingSailYardarmPortParentPositionX=studdingSailYardarmPortParent.localPosition.x;
				studdingSailYardarmStarboardParentPositionX=studdingSailYardarmStarboardParent.localPosition.x;
			}
			else
			{
				yardarmLOD0.parent = yardarmParentRotation;
				yardarmLOD1.parent = yardarmParentRotation;
				yardarmLOD2.parent = yardarmParentRotation;

				yardarmBlendshapesLOD0.parent = yardarmParentRotation;

				rearYardarmLOD0.parent = rearYardarmParentRotation;

				rearYardarmBlendshapesLOD0.parent = rearYardarmParentRotation;
				
				focLOD0.parent = focParentRotation;
				focEnLAirLOD0.parent = focEnLAirParentRotation;
				trinquetteLOD0.parent = trinquetteParentRotation;
				
				focBlendShapesLOD0.parent = focParentRotation;
				focEnLAirBlendShapesLOD0.parent = focEnLAirParentRotation;
				trinquetteBlendShapesLOD0.parent = trinquetteParentRotation;

				studdingYardarmPortLOD0.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD0.parent = studdingSailYardarmStarboardParent;
				studdingYardarmPortLOD1.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD1.parent = studdingSailYardarmStarboardParent;
				studdingYardarmPortLOD2.parent = studdingSailYardarmPortParent;
				studdingYardarmStarboardLOD2.parent = studdingSailYardarmStarboardParent;

				studdingSailPortBlendShapesLOD0.parent = studdingSailYardarmPortParent;
				studdingSailStarboardBlendShapesLOD0.parent = studdingSailYardarmStarboardParent;

		    	studdingSailYardarmPortParentPositionX=studdingSailYardarmPortParent.localPosition.x;
				studdingSailYardarmStarboardParentPositionX=studdingSailYardarmStarboardParent.localPosition.x;
			}	
		}	

		void MoveableRopesPositionSetup()
		{
			// don't mess with these:
			RopeEndPosA = new Vector3(0.0f, -2.9579f, -6.638f);
			RopeEndPosB = new Vector3(0.16f, -5.077f, -4.612f);
			RopeEndPos1A = new Vector3(0.0f, -3.6568f, -5.499f);
			RopeEndPos1B = new Vector3(0.063f, -3.6994f, -5.4411f);
			RopeEndPos2A = new Vector3(0.0f, -2.467f, -4.081f);
			RopeEndPos2B = new Vector3(0.01f, -4.7848f, -2.44f);
			RopeEndPos13A = new Vector3(6.708f, -20.061f, 0.277f);
			RopeEndPos13B = new Vector3(6.708f, -7.588f, 0.277f);
			RopeEndPos14A = new Vector3(-6.708f, -20.061f, 0.277f);
			RopeEndPos14B = new Vector3(-6.708f, -7.588f, 0.277f);

			// Jib topsail
			raisingEndPosition = new Vector3(0.0f, 0.0f, 0.0f);
			loweringEndPosition = new Vector3(-0.45f, -7.44f, 0.88f);
			raisingRope2Position = new Vector3(-2.768052f, 0.0f, 3.531254f);
			loweringRope2Position = new Vector3(-0.541f, 0.01f, 5.041f);
			//Jib
			raisingRope0Position = new Vector3(-0.0f, -2.9579f, -6.638f);
			loweringRope0Position = new Vector3(0.125f, -5.058f, -4.621f);

			Retracted = new Vector3(0.0f, 0.0f, 0.0f);
		}	

		public void ClearTransformMotion()
		{
			if(sailsLOD0_only==false) 
			{
				for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].ClearTransformMotion();
		    		SailClothLOD1[i].ClearTransformMotion();
		   		 	SailClothLOD2[i].ClearTransformMotion();
		   		}
		   		FlagClothLOD0.ClearTransformMotion();
		    	FlagClothLOD1.ClearTransformMotion();
		    	FlagClothLOD2.ClearTransformMotion();
		    }
		    else
		    {
		    	for(int i = 0; i < 9; i++)
		        {
					SailClothLOD0[i].ClearTransformMotion();
		   		}
		   		FlagClothLOD0.ClearTransformMotion();
		   	} 	

		}	

		void FadeOut(int nr)
		{
			if(sailsLOD0_only==false) 
			{
				SailClothLOD0[nr].SetEnabledFading(true, 1.4f);
				SailClothLOD1[nr].SetEnabledFading(true, 1.4f);
				SailClothLOD2[nr].SetEnabledFading(true, 1.4f);
			}
			else
			{
				SailClothLOD0[nr].SetEnabledFading(true, 1.4f);
			}	
		}	

		void FadeIn(int nr)
		{
			if(sailsLOD0_only==false) 
			{
				SailClothLOD0[nr].SetEnabledFading(false, 1.4f);
				SailClothLOD1[nr].SetEnabledFading(false, 1.4f);
				SailClothLOD2[nr].SetEnabledFading(false, 1.4f);
			}
			else
			{
				SailClothLOD0[nr].SetEnabledFading(false, 1.4f);
			}	
		}	
	}

}