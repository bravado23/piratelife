﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_FireCarronades : MonoBehaviour {

	public Hessburg.CutterControl CutterControl;
	private float Timer;
	public Transform Cutter;

	void Start () 
	{
		Timer=Time.time-2.5f;
	}
	
	void Update () 
	{
		Cutter.Translate(new Vector3(0.0f, 0.0f, 3.0f*Time.deltaTime));
		Cutter.Rotate(new Vector3(0.0f, Time.deltaTime, 0.0f));
		if(Timer+5.0f<Time.time)
		{
			Timer=Time.time;
			if(Random.Range(0.0f, 20.0f) > 14.0f) 
			{
				if(Random.Range(0.0f, 20.0f)>10.0f)
				{
					CutterControl.FireBroadsideStarboard();
				}
				else
				{
					CutterControl.FireBroadsidePort();
				}	
			}
			else
			{
				int num = (int) Mathf.Floor(Random.Range(0.0f, 13.99f));
				CutterControl.FireCannonNumber(num);
			}	
		}
	}
}
