﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_CameraRotationV2 : MonoBehaviour {

	void Start () {
	
	}
	
	void LateUpdate () 
	{
		CamMount.Rotate(new Vector3(0.0f, 10.0f* Time.deltaTime, 0.0f));
		Cam.transform.position=new Vector3(Cam.transform.position.x, Mathf.PingPong(Time.time*3.0f, 30.0f)+1.5f, Cam.transform.position.z);
		Cam.transform.LookAt(Target);
		Cam.fieldOfView=20.0f+Mathf.PingPong(Time.time*2.0f, 40.0f);
	}
	public Transform CamMount;
	public Camera Cam;
	public Transform Target;
}
