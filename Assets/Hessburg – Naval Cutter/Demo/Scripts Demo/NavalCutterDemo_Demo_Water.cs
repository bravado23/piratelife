﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_Demo_Water : MonoBehaviour 
{
	void Update () 
	{
		WaterMaterial.SetTextureOffset("_MainTex", new Vector2(Time.time*0.01f, -Time.time*0.2f));
	}

	public Material WaterMaterial;
}

