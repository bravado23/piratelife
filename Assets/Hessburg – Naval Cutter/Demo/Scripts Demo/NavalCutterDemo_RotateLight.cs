﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_RotateLight : MonoBehaviour {

	public Transform Horizontal;
	public Transform Vertical;
	public Light Sun;
	private float Timer;

	void Update () 
	{
		Horizontal.Rotate(new Vector3(0.0f, 3.0f* Time.deltaTime, 0.0f));
		Vertical.Rotate(new Vector3(8.0f* Time.deltaTime, 0.0f, 0.0f));

		if(Vertical.eulerAngles.x<0.0 || Vertical.eulerAngles.x >180.0) 
		{
			Sun.intensity=0.075f;
			Sun.color=new Color(0.8359375f, 0.890625f, 1.0f, 1.0f);
		}
		else
		{
			Sun.intensity=Mathf.Clamp(0.8f*Mathf.Clamp(Vertical.eulerAngles.x*5.0f, 0.0f, 1.0f), 0.075f, 1.0f);
			Sun.color=Color.Lerp(new Color(0.8359375f, 0.890625f, 1.0f, 1.0f), new Color(1.0f, 0.953125f, 0.8359375f, 1.0f), Mathf.Clamp(Vertical.eulerAngles.x*5.0f, 0.0f, 1.0f));
		}	

		if(Timer+1.0f<Time.time)
		{
			Timer=Time.time;
			DynamicGI.UpdateEnvironment();
		}
	}
}
