﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_CameraRotation : MonoBehaviour {

	public Camera Cam;
	public Transform Target;
	public Transform CameraMount;
	private Transform CameraTransform;
	private bool autoRotation;
	private float myTime;

	void Start () 
	{
		autoRotation=true;
		CameraTransform = Cam.transform;
	}

	void LateUpdate () 
	{
		if(autoRotation)
		{	
			myTime=myTime+Time.deltaTime;
			CameraMount.position=new Vector3(Target.position.x, Mathf.PingPong(myTime*3.2f, 90.0f)-30.0f, Target.position.z);
			CameraMount.Rotate(new Vector3(0.0f, 10.0f*Time.deltaTime, 0.0f));
			CameraTransform.LookAt(Target);
			Cam.fieldOfView=Mathf.PingPong(myTime*3.0f, 60.0f)+5.0f;
		}
	}

	public void AutoRotation(bool autoRot)
	{
			autoRotation = autoRot;
	}	
}

