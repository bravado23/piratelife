﻿using UnityEngine;
using System.Collections;

namespace Hessburg 
{
	public class NavalCutterDemo_ReloadTimeCounter : MonoBehaviour {

		public Hessburg.CutterControl CutterControl;
		private int remainingTime;
		public UnityEngine.UI.Text T;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update()
		{
			remainingTime = Mathf.CeilToInt(CutterControl.CannonsFXcontrol.ReloadTime - (Time.time-CutterControl.CannonsFXcontrol.Cannon[7].LastFired));
			if(remainingTime>0)
			{
				T.text="Reloading: ";
				T.text=T.text+remainingTime.ToString();
			}
			else
			{
				T.text="";
			}	
		}
	}
}	
