﻿using UnityEngine;
using System.Collections;

public class NavalCutterDemo_Example : MonoBehaviour
{
	public Hessburg.CutterControl CutterControl;

	void Start () 
	{
		CutterControl.WindSpeed(0.25f);
		CutterControl.WindDirection(0.0f);

		CutterControl.adjustNorth=0.0f;

		//CutterControl.OpenGunPort(0);
		//CutterControl.CloseGunPort(0);
		//CutterControl.OpenAllGunPorts();
		CutterControl.CloseAllGunPorts();
		//CutterControl.OpenStarboardGunPorts();
		//CutterControl.CloseStarboardGunPorts();
		//CutterControl.OpenPortGunPorts();
		//CutterControl.ClosePortGunPorts();
		//CutterControl.FireCannonNumber(0); 
		//CutterControl.FireBroadsideStarboard();
		//CutterControl.FireBroadsidePort();
		

		CutterControl.YardsAngle(0.4f);
		CutterControl.BoomAngle(0.0f);
		//CutterControl.HeadsailsAngle(-0.5f);
		CutterControl.RudderAngle(0.0f);

		CutterControl.DeckCollidersActive(false);	
/*
		CutterControl.SetJib(false);
		CutterControl.SetJibTopsail(false);
		CutterControl.SetStaysail(false);
		CutterControl.SetMainsail(false);
		CutterControl.SetSquareSail(false);
		CutterControl.SetTopSail(false);
		CutterControl.SetTopgallantSail(false);
		CutterControl.SetStuddingSailPort(false); 
		CutterControl.SetStuddingSailStarboard(false);
*/		
	}
}
