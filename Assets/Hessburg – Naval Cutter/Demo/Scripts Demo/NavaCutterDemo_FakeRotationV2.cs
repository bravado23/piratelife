﻿using UnityEngine;
using System.Collections;

public class NavaCutterDemo_FakeRotationV2 : MonoBehaviour 
{

	void Update () 
	{
		transform.localEulerAngles=new Vector3(Mathf.Sin(Time.time*0.6f)* Mathf.Rad2Deg*0.018f, 15.0f, Mathf.Sin(Time.time*0.2f)* Mathf.Rad2Deg*0.025f);
	}
}
