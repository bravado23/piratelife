﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Chat : Photon.MonoBehaviour
{
	#region Variables

	public static Chat Instance { get; private set; }

	[SerializeField]
	GameObject ChatBoxUI;

	InputField inputField;
	Text outputField;

	ArrayList messages;
	const int messageCount = 5;

	bool activeChat = false;

	#endregion

	#region Unity Methods

	private void Start()
	{
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
			return;
		}
		Instance = this;

		inputField = ChatBoxUI.transform.Find("InputField").GetComponent<InputField>();
		outputField = ChatBoxUI.transform.Find("OutputField").GetComponent<Text>();

		messages = new ArrayList(messageCount);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Return))
		{
			if (activeChat)
			{
				ChatBox(false);
				inputField.interactable = false;
				inputField.GetComponent<Image>().enabled = false;
			}
			else
			{
				ChatBox(true);
				inputField.interactable = true;
				inputField.GetComponent<Image>().enabled = true;
			}
		}
	}

	#endregion

	#region Methods

	public void ChatBox(bool isActive)
	{
		activeChat = isActive;

		inputField.characterLimit = 57;
		inputField.interactable = isActive;

		if (inputField.interactable)
		{
			inputField.ActivateInputField();
		}
		else
		{
			inputField.DeactivateInputField();

			if (inputField.text != "")
			{
				string message = PhotonNetwork.player.NickName + ": ";
				message += inputField.text;
				inputField.text = "";
				AddMessage(message, "#74DF00");
			}
		}
	}

	public void AddMessage(string message, string color = "#323232FF")
	{
		photonView.RPC("AddMessage_RPC", PhotonTargets.All, message, color);
	}

	public bool ActiveChat
	{
		get { return activeChat; }
	}

	#endregion

	#region RPC

	[PunRPC]
	public void AddMessage_RPC(string message, string color)
	{
		messages.Insert(messages.Count, "<color=" + color + ">" + message + "</color>");
		if (messages.Count > messageCount)
			messages.RemoveAt(0);

		outputField.text = "";
		foreach (string m in messages)
			outputField.text += m + "\n";
	}

	#endregion
}