﻿using UnityEngine;

public class Interpolated : Photon.MonoBehaviour
{
	#region Vars

	private Vector3 correctPlayerPos;
	private Quaternion correctPlayerRot;

    #endregion

    #region Serialize and Interpolate

    void Update ()
	{
        if (!photonView.isMine)
        {
            Interpolate();
        }
        else
            return;
       
    }

	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		}
		else
		{
            correctPlayerPos = (Vector3)stream.ReceiveNext ();
            correctPlayerRot = (Quaternion)stream.ReceiveNext ();
		}
	}

	void Interpolate ()
	{
		transform.position = Vector3.Lerp (transform.position, correctPlayerPos, Time.deltaTime * 9);
		transform.rotation = Quaternion.Lerp (transform.rotation, correctPlayerRot, Time.deltaTime * 9);
	}


    #endregion
}
