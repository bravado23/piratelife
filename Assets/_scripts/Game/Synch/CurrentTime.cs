﻿using UnityEngine;
using Ceto;

public class CurrentTime : Photon.MonoBehaviour, IOceanTime
{
    #region Vars

    [Header("Settings")]
    public float syncRate = 3f;
    public float lastUpdate = 0f;

    [Header("Current Time")]
    public float netTime;

    public static CurrentTime instance;
    public static float time;

    private float deltaTime;

    protected float lastUpdateTime = 0f;

    #endregion

    public float Now { get { return netTime; } }

    #region Methods

    void Awake()
    {
        instance = this;
        deltaTime = -(float)PhotonNetwork.time;
    }

    void Start()
    {
        if (Ocean.Instance != null)
            Ocean.Instance.OceanTime = this;
    }

    void Update()
    {

        time = (float)(PhotonNetwork.time + deltaTime);
        netTime = time;

        if (!PhotonNetwork.isMasterClient)
        {
            if (Time.time - syncRate > lastUpdate)
            {
                photonView.RPC("GetServerTime", PhotonTargets.MasterClient, PhotonNetwork.player);
                lastUpdate = Time.time;
            }
        }
    }

    #endregion

    #region RPC

    [PunRPC]
    void GetServerTime(PhotonPlayer sender)
    {
        photonView.RPC("SetDeltaTime", sender, time);
    }

    [PunRPC]
    void SetDeltaTime(float serverTime)
    {
        deltaTime = (float)((serverTime + (Time.time - lastUpdate) / 2) - PhotonNetwork.time);
        lastUpdateTime = (float)PhotonNetwork.time + deltaTime;
    }

    #endregion
}
