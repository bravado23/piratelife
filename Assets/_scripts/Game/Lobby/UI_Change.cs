﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Change : MonoBehaviour {

    public Text Name;
    public Text Strength;
    public Text IIntegrity;
    public Text Protectio;

    public void ChangeUI(string name, float strength, int integ, float protect)
    {
        Name.text = name.ToString();
        Strength.text = strength.ToString();
        IIntegrity.text = integ.ToString();
        Protectio.text = protect.ToString(); 
    }
}
