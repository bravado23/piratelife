﻿using AC.TimeOfDaySystemFree;
using System.Collections;
using UnityEngine;

public class BattleManager : Photon.MonoBehaviour
{
	public static BattleManager Instance { get; private set; }

	public GameObject TimeOfDayManager;
    public float windSpeed = 1f;

    private float newSpeed = 1f;
    private float newEulerAngles = 90f;
	private float currentTimeHour;
	private float currentTimeMinute;

    private void Awake()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, newEulerAngles);

        if (PhotonNetwork.isMasterClient)
        {
            StartCoroutine("SetWindSpeed");
            StartCoroutine("SetWindDirect");
        }
    }

	private void Start()
	{
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
			return;
		}
		Instance = this;
	}

    private void Update()
    {
		UpdateInfo();
    }

    private IEnumerator SetWindSpeed()
    {
        newSpeed = Random.Range(1f, 20f);

        yield return new WaitForSeconds(15f);

        StartCoroutine("SetWindSpeed");
    }
    private IEnumerator SetWindDirect()
    {
        newEulerAngles = Random.Range(0, 180);

        yield return new WaitForSeconds(35f);

        StartCoroutine("SetWindDirect");
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(newSpeed);
            stream.SendNext(newEulerAngles);
			stream.SendNext(TimeOfDayManager.GetComponent<TimeOfDayManager>().Hour);
			stream.SendNext(TimeOfDayManager.GetComponent<TimeOfDayManager>().Minute);
        }
        else
        {
            newSpeed = (float)stream.ReceiveNext();
            newEulerAngles = (float)stream.ReceiveNext();
			currentTimeHour = (float)stream.ReceiveNext();
			currentTimeMinute = (float)stream.ReceiveNext();
        }
    }

    private void UpdateInfo()
    {
        windSpeed = Mathf.Lerp(windSpeed, newSpeed, 0.5f * Time.deltaTime);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Lerp(transform.eulerAngles.y, newEulerAngles, 0.5f * Time.deltaTime));
		TimeOfDayManager.GetComponent<TimeOfDayManager>().Hour = currentTimeHour;
		TimeOfDayManager.GetComponent<TimeOfDayManager>().Minute = currentTimeMinute;
	}
}
