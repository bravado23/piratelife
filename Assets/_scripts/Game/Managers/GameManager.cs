﻿using ExitGames.Client.Photon;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Photon.MonoBehaviour
{
	public static GameManager Instance { get; private set; }

	[Header("UI Elementes")]
	public Text Ping;
	public Text WindSpeed;
	public Text WindTransform;
	public Text HealthUI;
	public GameObject MapObject;
    public GameObject RoomInfo;

	[Header("Spawners")]
	public GameObject[] Spawners;

    private GameObject Player;

    private bool _isLeaving;

    private void Start()
    {
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
			return;
		}
		Instance = this;
        
		SpawnPlayer();
    }

    private void Update()
    {
        if (_isLeaving)
            return;

        UpdateInput();
		UpdateUI();
    }

    private void UpdateInput()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !_isLeaving)
        {
			Chat.Instance.AddMessage(string.Format("Player {0} left the game.", PhotonNetwork.playerName), "#FF8C00");

			_isLeaving = true;
            PhotonNetwork.LeaveRoom();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            GetRoomInfo();
            RoomInfo.SetActive(!RoomInfo.activeSelf);
        }
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            RoomInfo.SetActive(!RoomInfo.activeSelf);

            for (int i = 0; i < RoomInfo.transform.Find("PlayerList").transform.childCount; i++)
                Destroy(RoomInfo.transform.Find("PlayerList").transform.GetChild(i).gameObject);
        }
    }

	private void UpdateUI()
	{
		Ping.text = string.Format("Ping - {0} ms.", PhotonNetwork.GetPing());
		WindSpeed.text = string.Format("Wind speed: {0} ms.", (int)BattleManager.Instance.windSpeed);
		WindTransform.text = string.Format("Wind direction: {0}° (Y)", (int)BattleManager.Instance.transform.eulerAngles.y);
	}

    private void OnLeftRoom()
    {
		float damageMoney = (GameCache.TotalDamage / 10);
		float killMoney = (GameCache.TotalKill * 2);
		int money = (int)damageMoney + (int)killMoney;

		Storage.PlayerMoney += money;
        Storage.PlayerKills += GameCache.TotalKill;
        Storage.PlayerDeaths += GameCache.TotalDeath;
		GameCache.ClearGameCache();

		Destroy(Player);

		Loading.Load(LoadingScene.Lobby);
	}

    private void SpawnPlayer()
    {
        PhotonNetwork.playerName = Storage.Login;

        SetPlayerProperties();

        var spawnId = Random.Range(0, Spawners.Length);

		Player = PhotonNetwork.Instantiate(string.Format("Prefabs/Ships/{0}", Config.CurrentShip), Spawners[spawnId].transform.position, Spawners[spawnId].transform.rotation, 0);
		MapObject.GetComponent<MapCanvasController>().playerTransform = Player.transform;
		Chat.Instance.AddMessage(string.Format("Player {0} join game.", PhotonNetwork.playerName), "#FF7F50");
	}

    private void GetRoomInfo()
    {
        RoomInfo.transform.Find("PlayerCount").GetComponent<Text>().text = "Player count: " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers;

        foreach(var p in PhotonNetwork.playerList)
        {
            var kills = new object();
            var deaths = new object();
            p.CustomProperties.TryGetValue("Kills", out kills);
            p.CustomProperties.TryGetValue("Deaths", out deaths);

            GameObject player = Instantiate(Resources.Load("UI/Player"), Vector3.zero, Quaternion.identity, RoomInfo.transform.Find("PlayerList").transform) as GameObject;
            player.transform.Find("PlayerName").GetComponent<Text>().text = p.NickName;
            player.transform.Find("PlayerKills").GetComponent<Text>().text = kills.ToString();
            player.transform.Find("PlayerDeaths").GetComponent<Text>().text = deaths.ToString();
        }
    }

    /// <summary>
    /// Set player properties
    /// </summary>
    public void SetPlayerProperties()
    {
        Hashtable PlayerStatistic = new Hashtable
        {
            { "Kills", GameCache.TotalKill },
            { "Deaths", GameCache.TotalDeath }
        };
        PhotonNetwork.player.SetCustomProperties(PlayerStatistic);
    }

	/// <summary>
	/// Set new player
	/// </summary>
	/// <param name="newPlayer">New player gameobject</param>
	public void SetPlayer (GameObject newPlayer)
	{
		Player = newPlayer;
	}
}
