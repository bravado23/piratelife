﻿using UnityEngine;
using System.Collections;
using Ceto;

public class BulletController : Photon.MonoBehaviour
{
	#region Public variables

	public PhotonPlayer Owner = null;

	public float Damage = 0f;

	#endregion

	#region Unity Methods

	private void Start()
    {
        StartCoroutine("Destroy");
    }

	private void Update()
	{
		Debug.DrawRay(transform.position, transform.forward, Color.red, 10f);

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, 3f))
		{
			if (hit.transform.tag == "Player")
			{
				if (PhotonNetwork.isMasterClient)
				{
					var receiver = hit.transform.gameObject;

                    if (receiver != null)
                    {
                        receiver.GetComponent<ShipController>().SendDamage(Damage, Owner, receiver.GetComponent<PhotonView>().owner);
                    }
                    else
                    {
                        Debug.LogWarningFormat("Receiver not found! Hit name - {0}", hit.transform.name);
                    }
				}

				// Particles impact
				GameObject particle = Instantiate(Resources.Load("Particles/ShipImpact"), transform.position, transform.rotation) as GameObject;
				particle.GetComponent<ParticleSystem>().Play();
				Destroy(particle.gameObject, 15f);

				Destroy(gameObject);
			}
		}

		// Splash if enter the water
		float seaHeight = Ocean.Instance.QueryWaves(transform.position.x, transform.position.z);
		if (transform.position.y < seaHeight)
		{
			GameObject particle = Instantiate(Resources.Load("Particles/WaterImpact"), transform.position, transform.rotation) as GameObject;
			particle.GetComponent<ParticleSystem>().Play();
			Destroy(particle.gameObject, 15f);

			Destroy(gameObject);
		}
	}

	#endregion

	#region Coroutines

	private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);    
    }

	#endregion

	#region Synch

	private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(Damage);
			stream.SendNext(Owner);
		}
		else
		{
			Damage = (float)stream.ReceiveNext();
			Owner = (PhotonPlayer)stream.ReceiveNext();
		}
	}

	#endregion
}
