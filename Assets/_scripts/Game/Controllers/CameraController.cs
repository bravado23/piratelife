﻿using UnityEngine;

public class CameraController : Photon.MonoBehaviour
{
	#region Public vars
	[Space(3)]
	[Header("Условия")]
	[Space(3)]

	[Space(3)]
	[Header("Свободная ли камера")]
	public bool FreeCamera = false;

	[Header("Проверка на препятствие")]
	public bool CheckObstacle = true;

	[Header("В лобби ли мы")]
	public bool isLobby = false;

	[Space(3)]
	[Header("Управление камерой от корабля")]
	[Space(3)]

	[Header("Таргет")]
    public Transform Target;

    [Header("Макс./Мин. дистанция от таргета")]
    public float maxViewDestance = 25f;
    public float minViewDistance = 1f;

    [Header("Скорость камеры")]
    public int zoomRate = 30;
    public float cameraTargetHeight = 1f;

	[Space(10)]
	[Header("Свободное управление камерой")]
	[Space(3)]

	[Header("Чувствительность мыши и скорость камеры")]
	public float mouseSensitivity = 3f;
	public float speed = 10f;

	[Header("Максимальный/Минимальный поворот камеры")]
	public float minimumX = -360f;
	public float maximumX = 360f;
	public float minimumY = -60f;
	public float maximumY = 60f;

	#endregion

	#region Private vars

	#region Variables for target controll

	private float x = 0.0f;
    private float y = 0.0f;

    private int mouseXSpeedMod = 5;
    private int mouseYSpeedMod = 3;
    
    private float distance = 10;

    private float desiredDistance;
    private float correctedDistance;
    private float currentDistance;

	#endregion

	#region Variables for free controll

	private Vector3 transfer;
	private Quaternion originalRotation;

	private float rotationX = 0F;
	private float rotationY = 0F;

	#endregion

	#endregion

	#region Unity Methods

	private void Start()
    {
		originalRotation = transform.rotation;

		if (!FreeCamera)
		{
			Vector3 angles = transform.eulerAngles;
			x = angles.x;
			y = angles.y;

			currentDistance = distance;
			desiredDistance = distance;
			correctedDistance = distance;
		}
		else
		{
			GetComponent<Camera>().orthographic = false;
		}
    }

    private void LateUpdate()
    {
		if (!isLobby && !photonView.isMine)
            gameObject.SetActive(false);
        else
        {
			if (!FreeCamera)
			{
				if (Input.GetMouseButton(1) || Input.GetKey(KeyCode.LeftAlt))
				{
					x += Input.GetAxis("Mouse X") * mouseXSpeedMod;
					y += Input.GetAxis("Mouse Y") * mouseYSpeedMod;
				}

				y = ClampAngle(y, -50, 80);

				Quaternion rotation = Quaternion.Euler(y, x, 0);

				desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs(desiredDistance);
				desiredDistance = Mathf.Clamp(desiredDistance, minViewDistance, maxViewDestance);
				correctedDistance = desiredDistance;

				Vector3 position = Target.position - (rotation * Vector3.forward * desiredDistance);

				RaycastHit collisionHit;
				Vector3 cameraTargetPosition = new Vector3(Target.position.x, Target.position.y + cameraTargetHeight, Target.position.z);

				bool isCorrected = false;
				if (CheckObstacle && Physics.Linecast(cameraTargetPosition, position, out collisionHit))
				{
					position = collisionHit.point;
					correctedDistance = Vector3.Distance(cameraTargetPosition, position);
					isCorrected = true;
				}

				currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomRate) : correctedDistance;

				position = Target.position - (rotation * Vector3.forward * currentDistance + new Vector3(0, -cameraTargetHeight, 0));

				transform.rotation = rotation;
				transform.position = position;
			}
			else
			{
				GetComponent<Camera>().orthographic = false;

				if (Input.GetKeyDown(KeyCode.LeftShift))
					speed *= 5f;
				if (Input.GetKeyUp(KeyCode.LeftShift))
					speed /= 5;

				rotationX += Input.GetAxis("Mouse X") * mouseSensitivity;
				rotationY += Input.GetAxis("Mouse Y") * mouseSensitivity;
				rotationX = ClampAngle(rotationX, minimumX, maximumX);
				rotationY = ClampAngle(rotationY, minimumY, maximumY);

				Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
				Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, Vector3.left);

				transform.rotation = originalRotation * xQuaternion * yQuaternion;

				transfer = transform.forward * Input.GetAxis("Vertical");
				transfer += transform.right * Input.GetAxis("Horizontal");

				transform.position += transfer * speed * Time.deltaTime;
			}
        }
    }

	#endregion

	#region Methods

	private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }

	private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){}

	#endregion
}