﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ShipController : Photon.MonoBehaviour
{
	#region Characteristics

	[Header("Can controll ship")]
	public bool CanControll = true;

    [Header("Smooth and Speed")]
    public float smoothRotate = 10f;
    public float specialSpeedShip = 100f;

    [Header("FireSystem")]
    public float Health = 100f;
	public float minDamage = 10f;
	public float maxDamage = 50f;

    public List<GameObject> LeftSpawn;
    public List<GameObject> RightSpawn;
    public List<GameObject> ForwardSpawn;
    public List<GameObject> BackSpawn;

    public ParticleSystem Smoke;

    public GameObject Bullet;
    public GameObject cam;

    [Header("Resources")]
    public int Ammunitions = 100;

	[Header("Audio")]
	public AudioClip Refresh;
	public AudioClip Fire;
	public AudioClip AudioDefeat;
	public AudioClip AudioWin;

	#endregion

	#region Private vars

	private bool isDie = false;
	private bool isStartDeathShip = false;

    private float speed;
    public int gear;

    private int stor;

    private int AmmoForFire;
    private int AmmoForRefresh;
    private float refreshDelay = 5f;
    public int side = 1;// 1-RIGHT, 2-LEFT, 0-FORWARD, 3-BACK

	private BattleManager bm = null;
	private PhotonPlayer lastDamagePlayer = null;

	private GameObject playerName;

	#endregion

	#region Unity Methods

	private void Start()
    {
        if (!photonView.isMine)
		{
			playerName = Instantiate (Resources.Load("UI/PlayerName"),
				GameObject.Find("WorldCanvas").transform) as GameObject;
			playerName.GetComponent<Text> ().text = photonView.owner.NickName;

            return;
        }

		GameObject.Find ("WorldCanvas").GetComponent<Canvas> ().worldCamera = cam.GetComponent<Camera>();

        AmmoForFire = RightSpawn.Count;
        AmmoForRefresh = RightSpawn.Count;

		bm = BattleManager.Instance;
    }

    private void Update()
    {
        if (photonView.isMine && CanControll)
        {
            ProccesInput();
            GearControll();
            SideChange();
            HealthController();
        }

		if (!photonView.isMine && !isDie)
		{
			if (Health <= 0)
			{
				isDie = true;
			}

			playerName.transform.position = this.transform.Find ("PName").position;
			playerName.transform.LookAt (GameObject.Find("WorldCanvas").GetComponent<Canvas>().worldCamera.transform);
			playerName.transform.Rotate(0, 180, 0);
		}

		if (isDie)
		{
			if (photonView.isMine)
				SinkShip ();
			else
				Destroy (playerName);

			if (!isStartDeathShip)
			{
				StartCoroutine("DeathShip", 30f);
				isStartDeathShip = true;
			}
		}
    }

    private void LateUpdate()
    {
        if (photonView.isMine && CanControll)
		{
            MoveForward();
        }
    }

    #endregion

    #region Inputs

    private void ProccesInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            GearUp();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            GearDown();
        }
        if (Input.GetKey(KeyCode.A))
        {
            MoveLeft();
        }
        if (Input.GetKey(KeyCode.D))
        {
            MoveRight();
        }
        if (Input.GetMouseButtonDown(0))
        {
            Shot();
        }
    }

    #endregion

    #region Controllers

    #region GearController

    private void GearUp()
    {
        if (gear < 3)
        {
            gear++;
        }
    }

    private void GearDown()
    {
        if (gear > 1)
        {
            gear--;
        }
    }

    private void GearControll()
    {
        switch (gear)
        {
            case 1:
                speed = Mathf.MoveTowards(speed, 0f, 0.3f * Time.deltaTime);
                break;
            case 2:
                speed = Mathf.MoveTowards(speed, SpeedSet() / 2, 1.2f * Time.deltaTime);
                break;
            case 3:
                speed = Mathf.MoveTowards(speed, SpeedSet(), 1.2f * Time.deltaTime);
                break;
        }

    }

    #endregion

    #region ShipControll Controller

    private void MoveForward()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void MoveRight()
    {
        if (speed > 1)
        {
            transform.Rotate(transform.up * smoothRotate * Time.deltaTime);
        }
    }

    private void MoveLeft()
    {
        if (speed>1)
        {
            transform.Rotate(transform.up * -smoothRotate * Time.deltaTime);
        }
    }

	#endregion

	#region Other controllers

	private void Shot()
    {
        if (AmmoForFire != 0)
        {
            StartCoroutine("Refresher");
            AmmoForFire = 0;

            switch (side)
            {
                case 0:
                    if (ForwardSpawn.Count > 0)
                        for (int i = 0; i < ForwardSpawn.Count; i++)
                            photonView.RPC("GetShot", PhotonTargets.All, i, Random.Range(0f, 3f), side);
                    break;
                case 1:
                    if (RightSpawn.Count > 0)
                        for (int i = 0; i < RightSpawn.Count; i++)
                            photonView.RPC("GetShot", PhotonTargets.All, i, Random.Range(0f, 3f), side);
                    break;
                case 2:
                    if (LeftSpawn.Count > 0)
                        for (int i = 0; i < LeftSpawn.Count; i++)
                            photonView.RPC("GetShot", PhotonTargets.All, i, Random.Range(0f, 3f), side);
                    break;
                case 3:
                    if (BackSpawn.Count > 0)
                        for (int i = 0; i < BackSpawn.Count; i++)
                            photonView.RPC("GetShot", PhotonTargets.All, i, Random.Range(0f, 3f), side);
                    break;
                default:
                    Debug.LogWarning("Unknown side to shot!");
                    break;
            }
        }
    }

	private void HealthController()
    {
		if (Health <= 0)
        {
			Health = 0;
			GameManager.Instance.HealthUI.text = "HP: 0";
			isDie = true;
        }

		GameManager.Instance.HealthUI.text = "Health: " + Mathf.Round(Health).ToString();
	}

	#endregion

	#endregion

	#region Math

	private float SpeedSet()
    {
        float a = (int)(Mathf.Abs(bm.transform.eulerAngles.y - transform.eulerAngles.y));
        float b = 360f - a;
        float c = Mathf.Min(a, b) * Mathf.PI / 180;
        float newSpeed = ((int)bm.windSpeed * Mathf.Cos(c)) + specialSpeedShip;
        if ((int)newSpeed <= 0)
            return specialSpeedShip;
        else return (int)newSpeed;
    }

    private void SideChange()
    {
        if (cam.transform.localEulerAngles.y >= 30 && cam.transform.localEulerAngles.y <= 160) side = 1;
        else if (cam.transform.localEulerAngles.y >= 210 && cam.transform.localEulerAngles.y <= 340) side = 2;
        else if (cam.transform.localEulerAngles.y < 30 && cam.transform.localEulerAngles.y > 0
            || cam.transform.localEulerAngles.y > 340 && cam.transform.localEulerAngles.y < 0) side = 0;
        else if (cam.transform.localEulerAngles.y > 160 && cam.transform.localEulerAngles.y < 210) side = 3;
    }

    #endregion

    #region Courutines

    private IEnumerator Refresher()
    {
        yield return new WaitForSeconds(refreshDelay);
        if (Ammunitions >= AmmoForRefresh)
        {
            Ammunitions -= AmmoForRefresh;
            AmmoForFire = AmmoForRefresh;
        }
        Debug.Log("Refresh");
    }

    /// <summary>
    /// Spawn bullet from cannon
    /// </summary>
    /// <param name="damage">Damage bullet</param>
    /// <param name="posX">Position to spawn X</param>
    /// <param name="posY">Position to spawn Y</param>
    /// <param name="posZ">Position to spawn Z</param>
    /// <param name="rotX">Rotation to spawn X</param>
    /// <param name="rotY">Rotation to spawn Y</param>
    /// <param name="rotZ">Rotation to spawn Z</param>
    /// <param name="rotW">Rotation to spawn W</param>
    private IEnumerator SpawnBullet(float damage, float wait, int cannonNumber, int side)
    {
        yield return new WaitForSeconds(wait);

        Vector3 pos;
        Quaternion rot;

        switch (side)
        {
            case 0:
                pos = new Vector3(ForwardSpawn[cannonNumber].transform.position.x, ForwardSpawn[cannonNumber].transform.position.y, ForwardSpawn[cannonNumber].transform.position.z);
                rot = new Quaternion(ForwardSpawn[cannonNumber].transform.rotation.x, ForwardSpawn[cannonNumber].transform.rotation.y, ForwardSpawn[cannonNumber].transform.rotation.z, ForwardSpawn[cannonNumber].transform.rotation.w);
                break;
            case 1:
                pos = new Vector3(RightSpawn[cannonNumber].transform.position.x, RightSpawn[cannonNumber].transform.position.y, RightSpawn[cannonNumber].transform.position.z);
                rot = new Quaternion(RightSpawn[cannonNumber].transform.rotation.x, RightSpawn[cannonNumber].transform.rotation.y, RightSpawn[cannonNumber].transform.rotation.z, RightSpawn[cannonNumber].transform.rotation.w);
                break;
            case 2:
                pos = new Vector3(LeftSpawn[cannonNumber].transform.position.x, LeftSpawn[cannonNumber].transform.position.y, LeftSpawn[cannonNumber].transform.position.z);
                rot = new Quaternion(LeftSpawn[cannonNumber].transform.rotation.x, LeftSpawn[cannonNumber].transform.rotation.y, LeftSpawn[cannonNumber].transform.rotation.z, LeftSpawn[cannonNumber].transform.rotation.w);
                break;
            case 3:
                pos = new Vector3(BackSpawn[cannonNumber].transform.position.x, BackSpawn[cannonNumber].transform.position.y, BackSpawn[cannonNumber].transform.position.z);
                rot = new Quaternion(BackSpawn[cannonNumber].transform.rotation.x, BackSpawn[cannonNumber].transform.rotation.y, BackSpawn[cannonNumber].transform.rotation.z, BackSpawn[cannonNumber].transform.rotation.w);
                break;
            default:
                yield break;
        }

		GameObject particle = Instantiate(Resources.Load("Particles/CannonFire"), pos, rot) as GameObject;
		particle.GetComponent<ParticleSystem>().Play();
		Destroy(particle.gameObject, 15f);

		GameObject bull = Instantiate(Resources.Load("Prefabs/Bullet"), pos, rot) as GameObject;
		bull.tag = "Bullet";
		bull.GetComponent<Rigidbody>().AddForce(bull.transform.forward * 2500 * Time.deltaTime, ForceMode.Impulse);
		bull.GetComponent<BulletController>().Damage = damage;
		bull.GetComponent<BulletController>().Owner = PhotonNetwork.player;
	}

	/// <summary>
	/// Kill ship
	/// </summary>
	/// <param name="time">Time to destroy ship</param>
	/// <returns></returns>
	private IEnumerator DeathShip(float time)
	{
		if (photonView.isMine)
		{
			if (lastDamagePlayer != null)
			{
				Chat.Instance.AddMessage(string.Format("Player [{0}] was sunk by [{1}].", PhotonNetwork.player.NickName, lastDamagePlayer.NickName), "#8B0000");
				photonView.RPC("GetKill", lastDamagePlayer);
			}
			else
			{
				Chat.Instance.AddMessage(string.Format("Player [{0}] drowned.", PhotonNetwork.player.NickName), "#C71585");
			}

			isDie = true;
			CanControll = false;

			var camera = transform.Find("Camera");
			camera.GetComponent<AudioSource>().PlayOneShot(AudioDefeat);
			camera.GetComponent<CameraController>().FreeCamera = true;
			camera.transform.SetParent(null);

			GameManager.Instance.SetPlayer(camera.gameObject);
            
            GameCache.TotalDeath += 1;

            GameManager.Instance.SetPlayerProperties();
        }

		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Rigidbody>().detectCollisions = false;

		Destroy(GetComponent<PhotonTransformView>());

		yield return new WaitForSeconds(time);
		PhotonNetwork.Destroy(gameObject);
	}

	#endregion

	#region Methods

	private void SinkShip()
	{
		gameObject.transform.Translate(Vector3.down * Time.deltaTime, Space.World);
	}

	public void SendDamage(float damage, PhotonPlayer Owner, PhotonPlayer receiver)
	{
		photonView.RPC("SendDamage", receiver, damage, Owner);
	}

	#endregion

	#region RPC

	[PunRPC]
	public void GetShot(int cannonNumber, float wait, int side)
	{
        StartCoroutine(SpawnBullet(Random.Range(minDamage, maxDamage), wait, cannonNumber, side));
	}
	[PunRPC]
	public void GetKill()
	{
		var camera = transform.Find("Camera");
		camera.GetComponent<AudioSource>().PlayOneShot(AudioWin);
		GameCache.TotalKill += 1;

        GameManager.Instance.SetPlayerProperties();
    }
	[PunRPC]
	public void GetDamage(float damage)
	{
		GameCache.TotalDamage += damage;
	}
	[PunRPC]
	public void SendDamage(float damage, PhotonPlayer Owner)
	{
		if (Health <= 0)
			return;

		// set here math for damage

		Health -= damage;
		lastDamagePlayer = Owner;
        photonView.RPC("GetDamage", Owner, damage);
    }

	#endregion

	#region Synch

	private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(Health);
		}
		else
		{
			Health = (float)stream.ReceiveNext();
		}
	}

	#endregion
}
