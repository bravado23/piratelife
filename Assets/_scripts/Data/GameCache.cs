﻿public class GameCache
{
	public static float TotalDamage = 0;
	public static int TotalKill = 0;
    public static int TotalDeath = 0;

	/// <summary>
	/// Clear game cache
	/// </summary>
	public static void ClearGameCache()
	{
		TotalDamage = 0;
		TotalKill = 0;
        TotalDeath = 0;
	}
}
