﻿public class Config
{
	public const string SceneLogin = "Login";
	public const string SceneLobby = "Lobby";
	public const string SceneGame = "Game";
	public const string SceneLoading = "Loading";

	public const string VersionGame = "0.06";

	public const string ServerUri = "https://moulin312.000webhostapp.com/";
	//public const string ServerUri = "https://destroy-five-bravado23.c9users.io/index.php";// debug

	public static string CurrentShip = "Galeon";
}