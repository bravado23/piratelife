﻿using UnityEngine;
using System.Collections;

public enum LoadingScene // Сцены которые требуют загрузки
{
	Lobby,
	Game,
}

public class Loading : MonoBehaviour
{
	private static LoadingScene _nextScene { get; set; } // Сцена которую нужно загрузить

	void Start ()
	{
		if (_nextScene == LoadingScene.Lobby) // Если нужно загрузить лобби
		{
			StartCoroutine (JoinLobby());
		}
	}

	private IEnumerator JoinLobby () // Загружаем лобби
	{
		while (PhotonNetwork.networkingPeer.State != ClientState.ConnectedToMaster) // Если мы ещё не подключились к комнате
			yield return new WaitForFixedUpdate ();

		PhotonNetwork.networkingPeer.OpJoinLobby (TypedLobby.Default); // Создаём лобби со стандартными настройками
	}

	void OnJoinedLobby() // Загружаем лобби
	{
		PhotonNetwork.LoadLevel (Config.SceneLobby);
	}

	void OnJoinedRoom() // Загружаем комнату
	{
		PhotonNetwork.LoadLevel (Config.SceneGame);
	}

	public static void Load (LoadingScene nextScene) // Переходим на следующую сцену
	{
		_nextScene = nextScene;

		PhotonNetwork.LoadLevel (Config.SceneLoading);
	}
}
