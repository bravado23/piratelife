﻿using UnityEngine;
using UnityEngine.UI;

public class Lobby : Photon.MonoBehaviour
{
	public Vector3 CurrentShipSpawn = new Vector3(7, 2, 58);
	
    private GameObject currentShip;

	public void Start()
	{
		StartCoroutine(NetworkApi.Instance.UpdateInfo());

		currentShip = Instantiate(Resources.Load("Prefabs/Ships/" + Config.CurrentShip), CurrentShipSpawn, Quaternion.identity)
			as GameObject;	
		Destroy (currentShip.GetComponent<ShipController>());
		Destroy (currentShip.transform.Find("Camera").gameObject);
		
		GameObject.FindWithTag("UI").GetComponent<UI_Change>().ChangeUI(currentShip.GetComponent<_ShipBase>().NameShip,
			currentShip.GetComponent<_ShipBase>().Strength, currentShip.GetComponent<_ShipBase>().Integrity,
			currentShip.GetComponent<_ShipBase>().Protection);
	}

	#region Buttons

	public void OnJoin() // При нажатии на вход
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 10;

		PhotonNetwork.JoinOrCreateRoom("Death Match Room", roomOptions, TypedLobby.Default); // Создаём новую или присоединяемся к существующей комнате с настройками

        Loading.Load(LoadingScene.Game); // Загружаем сцену игры
    }

	#region Shipyard

	public void SelectShip(string shipName)
	{
		if (currentShip.name == shipName)
			return;

		Config.CurrentShip = shipName;

		if (currentShip != null)
			Destroy(currentShip);

		currentShip = Instantiate(Resources.Load("Prefabs/Ships/" + shipName, typeof(GameObject)), CurrentShipSpawn, Quaternion.identity)
			as GameObject;
		Destroy (currentShip.GetComponent<ShipController>());
		Destroy (currentShip.transform.Find("Camera").gameObject);

		GameObject.FindWithTag("UI").GetComponent<UI_Change>().ChangeUI(currentShip.GetComponent<_ShipBase>().NameShip,
			currentShip.GetComponent<_ShipBase>().Strength, currentShip.GetComponent<_ShipBase>().Integrity,
			currentShip.GetComponent<_ShipBase>().Protection);

		Debug.Log(shipName);
	}

	#endregion

	#endregion
}
