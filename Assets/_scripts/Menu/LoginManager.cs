﻿using UnityEngine;
using UnityEngine.UI;

public class LoginManager : Photon.MonoBehaviour
{
	public static LoginManager Instance { get; private set; }

	public Text Version;

	[Header("Login fields")]
	public GameObject LoginFields;
	public GameObject errorLogin;
	public InputField login;
	public InputField password;

	[Space(20)]
	[Header("Register fields")]
	public GameObject RegisterFields;
	public GameObject errorRegister;
	public GameObject loginRegister;
	public InputField passwordRegister;
	public InputField ConfirmPasswordRegister;
	public InputField EmailRegister;
	
	[Header("Invite code")]
	public GameObject inviteCodeField;

	private void Start()
	{
		if (Instance != null && Instance != this)
		{
			Destroy(this);
		}
		Instance = this;

		PhotonNetwork.autoJoinLobby = false;
		PhotonNetwork.ConnectUsingSettings(Config.VersionGame);

		Version.text = string.Format ("Version: Build {0}", Config.VersionGame);
	}

	private void Update()
	{
		if (NetworkApi.Instance.IsLoggedIn)
		{
			Loading.Load(LoadingScene.Lobby);
		}
	}

	private void OnConnectedToMaster()
	{
		Debug.Log("Connected To Master");
	}

	public void ExitClick()
	{
		Application.Quit();
	}

	public void ClickToRegister()
	{
		LoginFields.SetActive(false);
		RegisterFields.SetActive(true);
	}

	public void ClickToLogin()
	{
		LoginFields.SetActive(true);
		RegisterFields.SetActive(false);
	}

	public void Auth()
	{
		if (login.text.Length < 4) { ShowLoginError("Name field short."); return; }
		if (login.text.Length > 10) { ShowLoginError("Name field long."); return; }
		if (password.text.Length < 6) { ShowLoginError("Password field short."); return; }
		if (password.text.Length > 16) { ShowLoginError("Password field long."); return; }

		StartCoroutine(NetworkApi.Instance.Login(login.text, password.text));
	}

	public void Register()
	{
		if (loginRegister.GetComponent<Text>().text.Length < 4) { ShowRegisterError("Name field short."); return; }
		if (loginRegister.GetComponent<Text>().text.Length > 10) { ShowRegisterError("Name field long."); return; }
		if (passwordRegister.text.Length < 6) { ShowRegisterError("Password field short."); return; }
		if (passwordRegister.text.Length > 16) { ShowRegisterError("Password field long."); return; }
		if (passwordRegister.text != ConfirmPasswordRegister.text) { ShowRegisterError("Password != ConfirmPassword."); return; }
		if (EmailRegister.text.Length < 4) { ShowRegisterError("Email field short."); return; }
		if (EmailRegister.text.Length > 50) { ShowRegisterError("Email field long."); return; }
		if (inviteCodeField.GetComponent<Text>().text.Length < 10) { ShowRegisterError("InviteCode field short."); return; }

		StartCoroutine(NetworkApi.Instance.Registration(loginRegister.GetComponent<Text>().text, passwordRegister.text,
			EmailRegister.text, inviteCodeField.GetComponent<Text>().text));
	}

	/// <summary>
	/// Show error in error(login) field
	/// </summary>
	/// <param name="errorText">Error text to input</param>
	public void ShowLoginError(string errorText)
	{
		errorLogin.GetComponent<Text>().text = errorText;
	}

	/// <summary>
	/// Show error in error(register) field 
	/// </summary>
	/// <param name="errorText">Error text to input</param>
	public void ShowRegisterError(string errorText)
	{
		errorRegister.GetComponent<Text>().text = errorText;
	}
}