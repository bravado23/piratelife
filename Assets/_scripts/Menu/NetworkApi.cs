﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NetworkApi : MonoBehaviour
{
	private static NetworkApi _instance;
	public static NetworkApi Instance { get { return _instance; } }

	public bool IsLoggedIn { get; private set; }

	private void Start()
	{
		_instance = this;
		DontDestroyOnLoad(gameObject);
	}

	private void Update()
	{
		if (PhotonNetwork.connectionState == ConnectionState.Disconnected)
		{
			SceneManager.LoadScene("FailedConnected");
		}
	}

	#region RequestToDataBase

    public IEnumerator Login(string login, string password) // Авторизация
    {
        var www = new WWW(string.Format("{0}?method=Auth&name={1}&password={2}", Config.ServerUri, login, password));

        if (!www.isDone)
            yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogException(new Exception("NetworkApi.Login error: " + www.error));
            SceneManager.LoadScene("FailedConnected");
            yield break;
        }

        var pars = www.text.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
        var dic = pars.Select(n => n.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries)).ToDictionary(k => k[0], v => v[1]);

        if (dic["name"] == "0")
        {
			var error = dic["error"];
			LoginManager.Instance.ShowLoginError(error);
            yield break;
        }

		Storage.Login = dic["name"];
		Storage.PlayerMoney = int.Parse(dic["money"]);
		Storage.PlayerKills = int.Parse(dic["kills"]);
		Storage.PlayerDeaths = int.Parse(dic["deaths"]);

		IsLoggedIn = true;
    }

	// Registration
	public IEnumerator Registration(string login, string password, string email, string inviteCode) // Регистрация
	{
		var www = new WWW(string.Format("{0}?method=Registration&email={1}&name={2}&password={3}&code={4}", Config.ServerUri, email, login, password, inviteCode));

		if (!www.isDone)
			yield return www;

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogException(new Exception("NetworkApi.Registration error: " + www.error));
			SceneManager.LoadScene("FailedConnected");
			yield break;
		}

		var pars = www.text.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
		var dic = pars.Select(n => n.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries)).ToDictionary(k => k[0], v => v[1]);

		if (dic["name"] == "0")
		{
			var error = dic["error"];
			LoginManager.Instance.ShowRegisterError(error);
			yield break;
		}

		Storage.Login = dic["name"];
		Storage.PlayerMoney = int.Parse(dic["money"]);
		Storage.PlayerKills = int.Parse(dic["kills"]);
		Storage.PlayerDeaths = int.Parse(dic["deaths"]);

		IsLoggedIn = true;
	}

	// Update main statistic
	public IEnumerator UpdateInfo()
	{
		var www = new WWW(string.Format("{0}?method=Update&name={1}&kills={2}&deaths={3}&money={4}",
			Config.ServerUri,
			Storage.Login,
			Storage.PlayerKills,
			Storage.PlayerDeaths,
			Storage.PlayerMoney));

		if (!www.isDone)
			yield return www;

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogException(new Exception("NetworkApi.UpdateInfo error: " + www.error));
			yield break;
		}

		Debug.Log("NetworkApi.UpdateInfo result: " + www.text);
	}

    #endregion

}